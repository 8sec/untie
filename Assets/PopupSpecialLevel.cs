﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PopupSpecialLevel : MonoBehaviour
{
    [SerializeField] private Image _giftImage;
    // Start is called before the first frame update
    void Start()
    {
        LeanTween.scale(_giftImage.gameObject, Vector3.one * 1.1f, 0.75f).setLoopPingPong(-1);
    }

    public void HidePopup()
    {
        LeanTween.scale(_giftImage.gameObject, Vector3.zero, 0.3f).setEaseInBack().setOnComplete(c => gameObject.SetActive(false));
    }
}
