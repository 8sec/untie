﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObstacleManager : MonoBehaviour
{
    public static ObstacleManager Instance;
    [SerializeField] private List<GameObject> _obstacles = new List<GameObject>();
    private int _currentObstacledIndex = -1;
   

    private void Awake()
    {
        Instance = this;
        DisableAllObstacles();
    }

    private void DisableAllObstacles()
    {
        for (int i = 0; i < _obstacles.Count; i++)
        {
            _obstacles[i].SetActive(false);
        }

        _currentObstacledIndex = -1;
    }

    public ObstacleInfo GetCurrentObstacleInfos()
    {
       ObstacleInfo obstacleInfos = new ObstacleInfo();
        obstacleInfos.obstacleIndex = _currentObstacledIndex;

        if (obstacleInfos.obstacleIndex < 0)
        {
            return obstacleInfos;
        }


        Transform currentObstacle = _obstacles[_currentObstacledIndex].transform;

        obstacleInfos.overridePosition = currentObstacle.position;
        obstacleInfos.overrideRotation = currentObstacle.eulerAngles;

        return obstacleInfos;
    }

    public void InitializeObstacles(ObstacleInfo infos)
    {
        _currentObstacledIndex = infos.obstacleIndex;
        if (infos.obstacleIndex < 0 || infos.obstacleIndex > _obstacles.Count)
        {
            Debug.LogError("RETURNING, index is " + infos.obstacleIndex);
            return;
        }


        _obstacles[infos.obstacleIndex].SetActive(true);
        _currentObstacledIndex = infos.obstacleIndex;

        GameObject g = _obstacles[infos.obstacleIndex];
        // LeanTween.move(g, infos.overridePosition, 0.5f).setDelay(1f);
        // LeanTween.rotate(g, infos.overrideRotation, 0.5f).setDelay(1f);
        _obstacles[infos.obstacleIndex].transform.position = infos.overridePosition;
        _obstacles[infos.obstacleIndex].transform.eulerAngles = infos.overrideRotation;
        Obi.ObiCollider a = g.GetComponent<Obi.ObiCollider>();
        g.SetActive(false);
        g.SetActive(true);

        foreach (var col in GetComponentsInChildren<Obi.ObiCollider>())
        {
            col.gameObject.SetActive(false);
            col.gameObject.SetActive(true);
        }

      
    }

#if UNITY_EDITOR

    private void Update()
    {
        if (Input.GetMouseButtonDown(1))
        {
            _currentObstacledIndex++;

            if (_currentObstacledIndex > _obstacles.Count - 1)
            {
                _currentObstacledIndex = 0;
            }

            for (int i = 0; i < _obstacles.Count; i++)
            {
                _obstacles[i].SetActive(i == _currentObstacledIndex ? true : false);
            }
        }

        if (Input.GetKeyDown(KeyCode.Space))
        {
            DisableAllObstacles();
        }
    }

#endif
}

[System.Serializable]
public class ObstacleInfo
{
    public int obstacleIndex = -1;
    public Vector3 overridePosition;
    public Vector3 overrideRotation;
}
