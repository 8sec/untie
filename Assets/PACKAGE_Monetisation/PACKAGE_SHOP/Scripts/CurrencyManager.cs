﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using UnityEngine.UI;

public class CurrencyManager : MonoBehaviour
{

    public static CurrencyManager Instance;

    public int totalCurrency;
    public Text currencyAmountText;
    public GameObject currencyDisplayerPannel;

    

    private const string CurrencyAmountKey = "CoinKey";

    private void Awake()
    {
        Instance = this;
        totalCurrency = PlayerPrefs.GetInt(CurrencyAmountKey, 0);
        UpdateCurrencyAmountText(totalCurrency);
    }

   
    public void UpdateCurrencyAmountText(int currencyAmount)
    {
        if (currencyAmountText != null)
        {
            currencyAmountText.text = currencyAmount.ToString();
        }
    }

    public void AddCurrency(int currencyAmountToAdd)
    {
        totalCurrency += currencyAmountToAdd;
        UpdateCurrencyAmountText(totalCurrency);
      //  LeanTween.scale(currencyDisplayerPannel, Vector3.one * 1.5f, 0.1f).setLoopPingPong(1);
        PlayerPrefs.SetInt(CurrencyAmountKey, totalCurrency);
    }

    public bool RemoveCurrency(int currencyAmountToRemove)
    {
        if (totalCurrency >= currencyAmountToRemove)
        {
            totalCurrency -= currencyAmountToRemove;
            UpdateCurrencyAmountText(totalCurrency);
           //LeanTween.scale(currencyDisplayerPannel, Vector3.one * 1.5f, 0.1f).setLoopPingPong(1);
            PlayerPrefs.SetInt(CurrencyAmountKey, totalCurrency);
            return true;
        }
        return false;

    }

    public void HideCurrencyDisplayer()
    {
        currencyDisplayerPannel.SetActive(false);
    }

    public void ShowCurrencyDisplayer()
    {
        currencyDisplayerPannel.SetActive(true);
    }
}
