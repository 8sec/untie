﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class GroundManager : MonoBehaviour
{
    public static GroundManager Instance;
    [SerializeField] private List<GameObject> _grounds = new List<GameObject>();
    private int _currentGroundIndex = 0;

    public int CurrentGroundIndex => _currentGroundIndex;

    private void Awake()
    {
        Instance = this;
        DisableAllGrounds();
    }

    

    private void DisableAllGrounds()
    {
        for (int i = 0; i < _grounds.Count; i++)
        {
            _grounds[i].SetActive(false);
        }
    }

    public void InitializeGround(int groundIndex)
    {
        _grounds[groundIndex].SetActive(true);
        _currentGroundIndex = groundIndex;
    }

    [Button("NEXT GROUND")]
    public void IncrementGround()
    {
        DisableAllGrounds();

        _currentGroundIndex++;

        if (_currentGroundIndex > _grounds.Count - 1)
        {
            _currentGroundIndex = 0;
        }

        _grounds[_currentGroundIndex].SetActive(true);

    }
}
