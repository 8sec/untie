﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class ShopItem_Material : ShopItem
{
    [SerializeField] private Material _material;
    [SerializeField] private bool _isScrollingMaterial;
    [ShowIf("_isScrollingMaterial")]
    [SerializeField] private float _scrollSpeed;

    public Material Material => _material;
    public bool IsScrolling => _isScrollingMaterial;
    public float ScrollSpeed => _scrollSpeed;

    public override void Init()
    {
        base.Init();
        MeshRenderer rend = GameObject.CreatePrimitive(PrimitiveType.Sphere).GetComponent<MeshRenderer>();
        rend.transform.parent = transform.GetChild(1);
        rend.material = _material;
        rend.transform.localPosition = Vector3.zero;
        rend.transform.rotation = transform.GetChild(1).rotation;
        rend.transform.localScale = Vector3.one;
    }

    public override void Action()
    {
        base.Action();
        Debug.LogError("Clicked On Material");
    }
}
