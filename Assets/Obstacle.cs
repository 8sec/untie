﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Obstacle : MonoBehaviour
{
    [SerializeField] private Material _baseMat;
    [SerializeField] private Material _feedbackMat;
    [SerializeField] private MeshRenderer _renderer;

    public void OnCollision()
    {
        _renderer.material = _feedbackMat;
    }

    public void OnNoCollision()
    {
        _renderer.material = _baseMat;
    }
   

}
