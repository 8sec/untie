﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using GameAnalyticsSDK;
public class BuyNoAdsButton : MonoBehaviour {

    public static int LastIAPOpenedDay
    {
        get
        {
            return PlayerPrefs.GetInt("LastIAPOpenedDay", -1);
        }
        set
        {
            PlayerPrefs.SetInt("LastIAPOpenedDay", value);

        }
    }

    public GameObject SALE_GO;

    // Start is called before the first frame update
    void Start () {
        if (UserConfig.NoAds)
        {
            gameObject.SetActive(false);
            return;
        }

        if(SALE_GO!=null)
        {
            SALE_GO.SetActive(MonetizationManager.IAPSale);
        }
    }

    public void Action () {
   
         IAPManager.Instance.Buy_NoAds_Normal();
    }
}