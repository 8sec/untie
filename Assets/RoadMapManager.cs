﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class RoadMapManager : MonoBehaviour
{

    [SerializeField] private Image[] _stepsImages;
    [SerializeField] private Color _greenColor;
    [SerializeField] private Color _yellowColor;
    [SerializeField] private GameObject _popupPremiumLevel;
    private int currentLevel = 1;

    LTDescr _scaleLT;

   

    private void Start()
    {
        currentLevel = LevelManager.Instance.CurrentLevelIndex;
        if (currentLevel > _stepsImages.Length)
        {
            currentLevel = currentLevel % _stepsImages.Length;
        }
        InitWorldMap();
    }


    private void InitWorldMap()
    {
        int actualLvl = currentLevel - 1;

        if ((actualLvl + 1) % 10 == 0)
        {
            _popupPremiumLevel.SetActive(true);
            _popupPremiumLevel.transform.localScale = Vector3.zero;
            LeanTween.scale(_popupPremiumLevel, Vector3.one, 0.4f).setEaseOutBack();
        }

        for (int i = 0; i < _stepsImages.Length; i++)
        {
            if (i < actualLvl)
            {
                _stepsImages[i].color = _greenColor;
            }

            else if (i == actualLvl)
            {
                _stepsImages[i].color = _yellowColor;
                _scaleLT =  LeanTween.scale(_stepsImages[i].gameObject, _stepsImages[i].transform.localScale + Vector3.one * 0.25f, 0.5f).setLoopPingPong(-1);
            }

            else
            {
                return;
            }
        }
    }

    private void HideWorldMap()
    {
        LeanTween.cancel(_scaleLT.uniqueId);
        LeanTween.scale(gameObject, Vector3.zero, 0.3f).setEaseInBack().setOnComplete(c => gameObject.SetActive(false));
    }

    private void Update()
    {
        if (GameManager.Instance.CurrentGameState == GameState.INITIAL_STATE)
        {
            if (Input.GetMouseButtonDown(0))
            {
                HideWorldMap();
            }
        }
    }



}
