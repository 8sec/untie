﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnchorMove : MonoBehaviour
{
    public float force = 100F;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            GetComponent<Rigidbody>().AddForce(Vector3.right * force);
        } 
    }
}
