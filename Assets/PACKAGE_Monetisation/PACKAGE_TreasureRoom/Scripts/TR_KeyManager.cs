﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;


public class TR_KeyManager : MonoBehaviour
{
    [SerializeField] private bool _showKeysDuringGameplay;

    public static TR_KeyManager Instance;

    [SerializeField] private TR_PannelTreasureRoom_KeyDisplayer _chestRoomPannel;
    [ShowIf("_showKeysDuringGameplay")]
    [SerializeField] private TR_GameplayKeyDisplayer _gameplayKeyDisplayer;
    [SerializeField] private TR_RoadToChestRoomOverlay _roadToChestRoomOverlay;
    [SerializeField] private int _maxKeyNumber;

    private bool _hasPickedUpKeyThisLevel;

    public bool HasPickedUpKeyThisLevel => _hasPickedUpKeyThisLevel;

    public int MaxKeyNumber => _maxKeyNumber;

    private int _currentKeyNumber;
    public int CurrentKeyNumber => _currentKeyNumber;

    public bool HasKeyLeft { get { return _currentKeyNumber > 0; } }

    public bool CanOpenChestRoom { get { return CurrentKeyNumber >= MaxKeyNumber; } }


    private const string CURRENT_TREASURE_KEY_NUMBER_KEY = "CurrentTreasureKeyNumber";


    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }

    private void Start()
    {
        _currentKeyNumber = PlayerPrefs.GetInt(CURRENT_TREASURE_KEY_NUMBER_KEY, 0);

        if (_showKeysDuringGameplay == true)
        {
            _gameplayKeyDisplayer.UpdateKeyDisplay(_currentKeyNumber);
        }
    }

    public void DisableRoadChest()
    {
        if (_roadToChestRoomOverlay != null)
        {
            _roadToChestRoomOverlay.gameObject.SetActive(false);
        }
    }

    public void OnKeyPickedUp()
    {
        _hasPickedUpKeyThisLevel = true;
    }

    public void EnableRoadChest()
    {
        if (_roadToChestRoomOverlay == null)
        {
            return;
        }
        _roadToChestRoomOverlay.gameObject.SetActive(true);
        Debug.LogError("THER");
        _roadToChestRoomOverlay.UpdateDisplay(_currentKeyNumber);
    }

    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.UpArrow))
        {
            OpenChestRoom();
        }
    }



    [Button("Add Key")]
    public void AddKey(int keysToAdd)
    {
        if (_currentKeyNumber < _maxKeyNumber)
        {
            _currentKeyNumber += keysToAdd;

            if (_currentKeyNumber > _maxKeyNumber)
            {
                _currentKeyNumber = _maxKeyNumber;
            }

            if (_showKeysDuringGameplay == true)
            {
                _gameplayKeyDisplayer.UpdateKeyDisplay(_currentKeyNumber);
            }

            _chestRoomPannel.UpdateKeyDisplay(_currentKeyNumber);
            if (_roadToChestRoomOverlay != null)
            {
                _roadToChestRoomOverlay.UpdateDisplay(_currentKeyNumber);
            }

            PlayerPrefs.SetInt(CURRENT_TREASURE_KEY_NUMBER_KEY, _currentKeyNumber);

        }

        else
        {
            Debug.LogError("Can't add more key : current key number would be higher than max key number");
        }
    }

    public void SaveKeyPrefab()
    {
        PlayerPrefs.SetInt(CURRENT_TREASURE_KEY_NUMBER_KEY, _currentKeyNumber);
    }

    public void UseKey()
    {
        if (_currentKeyNumber > 0)
        {
            _currentKeyNumber--;
            _chestRoomPannel.UpdateKeyDisplay(_currentKeyNumber);
            if (_roadToChestRoomOverlay != null)
            {
                _roadToChestRoomOverlay.UpdateDisplay(_currentKeyNumber);
            }
            PlayerPrefs.SetInt(CURRENT_TREASURE_KEY_NUMBER_KEY, _currentKeyNumber);
        }

        else
        {
            Debug.LogError("Can't use any key : current key number = 0");
        }
    }

    public void OpenChestRoom()
    {
        Debug.LogError("OPENING CHEST ROOM");
        UIController.Instance.ShowPanelInstantly(UIPanelName.TREASURE_ROOM,true);
        
        if (_roadToChestRoomOverlay != null)
        {
            _roadToChestRoomOverlay.gameObject.SetActive(false);
        }

        AddKey(3);
    }

    public void CloseChestRoom()
    {
        Application.LoadLevel(Application.loadedLevel);
    }
    
}
