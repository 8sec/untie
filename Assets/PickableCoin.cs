﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PickableCoin : MonoBehaviour
{


    private void Start()
    {
        gameObject.SetActive(false);
        gameObject.SetActive(true);
    }

    public void TakeCoin()
    {
        LeanTween.scale(gameObject, Vector3.zero, 0.4f).setEaseOutBack();
        GetComponent<Obi.ObiCollider>().enabled = false;
        GetComponent<Collider>().enabled = false;
        VibrationManager.VibrateLight();
        CoinManager.Instance.SpawnFXAtPos(transform.position);
        CoinManager.Instance.AddCoin();
    }
}
