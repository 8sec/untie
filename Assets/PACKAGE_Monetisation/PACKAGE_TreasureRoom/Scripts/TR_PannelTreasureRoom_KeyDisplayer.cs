﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TR_PannelTreasureRoom_KeyDisplayer : MonoBehaviour
{
    [Header("Leantween Params")]
    [SerializeField] private float _homeButtonScaleUpTime = 0.2f;
    [SerializeField] private LeanTweenType _homeButtonEaseType = LeanTweenType.easeOutBack;

    [SerializeField] private Image[] _availabeKeys;
    [SerializeField] private GameObject _rewardedButtonMoreKeys;
    [SerializeField] private GameObject _buttonNoThanksMoreKeys;
    [SerializeField] private GameObject _keysRemainingDisplay;
    [SerializeField] private GameObject _homeButton;

   

    private void OnEnable()
    {
        _rewardedButtonMoreKeys.SetActive(false);
        _buttonNoThanksMoreKeys.SetActive(false);
        _homeButton.SetActive(false);
        for (int i = 0; i < _availabeKeys.Length; i++)
        {
            _availabeKeys[i].enabled = true;
        }
    }

   public void UpdateKeyDisplay(int enableKeysRemaining)
   {
        if (gameObject.activeSelf == false)
        {
            return;
        }

        for (int i = 0; i < _availabeKeys.Length; i++)
        {
            if (i >= enableKeysRemaining)
            {
                _availabeKeys[i].enabled = false;
            }

            else
            {
                _availabeKeys[i].enabled = true;
            }
        }

      
            UpdateRewardedButtonDisplay(enableKeysRemaining);
   }

    public void UpdateRewardedButtonDisplay(int remainingKeys)
    {
        if(TR_ChestManager.Instance.AllChestsAreUnlocked() == true)
        {
            _rewardedButtonMoreKeys.SetActive(false);
            _buttonNoThanksMoreKeys.SetActive(false);
            _keysRemainingDisplay.SetActive(false);
            _homeButton.SetActive(true);
            _homeButton.transform.localScale = Vector3.zero;
            LeanTween.scale(_homeButton, Vector3.one, _homeButtonScaleUpTime).setEase(_homeButtonEaseType);
        }

        else
        {
            if (remainingKeys > 0)
            {
                _keysRemainingDisplay.SetActive(true);
                _rewardedButtonMoreKeys.SetActive(false);
                _buttonNoThanksMoreKeys.SetActive(false);
            }

            else
            {
                _keysRemainingDisplay.SetActive(false);
                _rewardedButtonMoreKeys.SetActive(true);
                _buttonNoThanksMoreKeys.SetActive(true);
            }
        }
    }
}
