﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TR_ChestManager : MonoBehaviour
{
    public static TR_ChestManager Instance;

    [SerializeField] private TR_Chest[] _allChests;
    [SerializeField] private int[] _possibleMoneyAmoutInChests;
    [SerializeField] private Transform _bestRewardSpawn;

    GameObject bestRewardGO;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
    }



    private void Start()
    {
        InitializeChests();
        SetBestReward();
    }

    

    private void InitializeChests()
    {
        int randomIndex = Random.Range(0, _allChests.Length);

        for (int i = 0; i < _allChests.Length; i++)
        {
            if (i == randomIndex)
            {
                //FT_CI_Car bestReward = FT_ShopManager.Instance.GetRandomHat();
                ShopItem_Spawnable item = (ShopItem_Spawnable) ShopManager.Instance.GetRandomItemNotUnlocked(ShopCategoryItem.Handle);

                //bestRewardGO = bestReward.CarGO;
                bestRewardGO = item.ObjectToSpawn.gameObject;
                //_allChests[i].SetSkinChest(bestReward);
                _allChests[i].SetSkinChest(item);
            }

            else
            {
                int randomAmoutOfMoney = _possibleMoneyAmoutInChests[Random.Range(0, _possibleMoneyAmoutInChests.Length)];
                _allChests[i].SetMoneyChest(randomAmoutOfMoney);
            }
        }
    }

    private void SetBestReward()
    {
        Transform t = Instantiate(bestRewardGO, _bestRewardSpawn.position, Quaternion.identity, _bestRewardSpawn).transform;
        t.gameObject.SetActive(true);
        t.localEulerAngles = Vector3.zero.SetX(-18);
        t.localScale = Vector3.one * 1.5f;
        RotationScript s = t.gameObject.AddComponent<RotationScript>();
        s.rotationSpeed = Vector3.up * 50;

    }

    public bool AllChestsAreUnlocked()
    {
        bool allChestsAreUnlocked = true;

        for (int i = 0; i < _allChests.Length; i++)
        {
            if (_allChests[i].HasBeenRecolted == false)
            {
                allChestsAreUnlocked = false;
                break;
            }
        } 

        return allChestsAreUnlocked;
    }
}
