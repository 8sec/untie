﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent(typeof(Image))]
public class SpriteSlideshow : MonoBehaviour
{
    
    public List<Sprite> ImagesToShow = new List<Sprite>();
    public bool ShuffleSlideshow = false;
    private int currentImageIndex = 0;

    public float TimeBetweenSlides = 0.5f;
    Image m_Image;
    private void Awake()
    {
        m_Image = GetComponent<Image>();
        if (ShuffleSlideshow)
            ImagesToShow.Shuffle();
        InvokeRepeating("ShowNextSlide", 0f, TimeBetweenSlides);
    }

    void ShowNextSlide()
    {
        if (ImagesToShow.Count == 0)
        {
            StopSlideShow();
            Debug.LogError("Slideshow has no images to show");
            return;
        }
        currentImageIndex++;
        if (currentImageIndex > ImagesToShow.Count - 1)
        {
            currentImageIndex = 0;
            if (ShuffleSlideshow)
                ImagesToShow.Shuffle();
        }
        m_Image.sprite = ImagesToShow[currentImageIndex];

        ChangeSlideAnimation();
    }

    void StopSlideShow()
    {
        CancelInvoke("ShowNextSlide");
    }
    

    protected virtual void ChangeSlideAnimation()
    {
        LeanTween.moveLocalX(gameObject, transform.localPosition.x + 10f, 0.05f).setLoopPingPong(1);
    }
}
