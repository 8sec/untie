﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DEBUG_ObjectRandomPlacement : MonoBehaviour {
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    [ContextMenu ("Set Random Rotation")]
    public void SetRandomRotation () {
        transform.localEulerAngles = new Vector3 (Random.Range (0f, 360f), Random.Range (0f, 360f), Random.Range (0f, 360f));

    }
}