﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationCompensator : MonoBehaviour
{
    private Quaternion defaultRotation;
    private void Awake()
    {
        transform.GetChild(0).Rotate(-Vector3.forward * 50);
        defaultRotation = transform.rotation;
    }
    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = defaultRotation;
    }
}
