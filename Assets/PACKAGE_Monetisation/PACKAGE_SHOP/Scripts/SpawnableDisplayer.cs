﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class SpawnableDisplayer : MonoBehaviour
{

    [SerializeField] private ShopCategoryItem _displayCategory;

    private bool IsHandle => _displayCategory == ShopCategoryItem.Handle;

    [ShowIf("IsHandle")]
    [SerializeField] private EquipedItemsDisplayer _displayer;

    public void UpdateDisplay()
    {
        if (transform.childCount > 0)
        {
            for (int i = 0; i < transform.childCount; i++)
            {
                Destroy(transform.GetChild(i).gameObject);
            }
        }

        ShopItem item = ShopManager.Instance.GetCurrentlySelectedItem(_displayCategory);

        switch (_displayCategory)
        {
            
            case ShopCategoryItem.Handle:
               
                ShopItem_Spawnable spawnableItem = (ShopItem_Spawnable)item;
             
                if (spawnableItem.ObjectToSpawn == null)
                {
                    Debug.LogError("OBJECT TO SPAWN IS NOT SET, IT IS ONLY OK IF YOU WANT TO SET THE SKIN TO NO SKIN");
                    return;
                }
                Color c = _displayer.Material.color;
                GameObject go = Instantiate(spawnableItem.ObjectToSpawn, transform.position, transform.rotation, transform);
               
                if (go.GetComponentInChildren<Renderer>() != null)
                {
                    go.GetComponentInChildren<Renderer>().material.color = c;
                }


                break;
            default:
                Debug.LogError("Display Category is invalid on the Spawnable Displayer " + gameObject.name);
                break;
        }
    }

 

  // public void OnShopExit()
  // {
  //     ShopItem item = ShopManager.Instance.GetCurrentlySelectedItem(_displayCategory);
  //
  //     if (item == null)
  //     {
  //         return;
  //     }
  //
  //     if (item.IsUnlocked == false)
  //     {
  //         if (transform.childCount > 0)
  //         {
  //             for (int i = 0; i < transform.childCount; i++)
  //             {
  //                 Destroy(transform.GetChild(i).gameObject);
  //             }
  //         }
  //     }
  // }

}
