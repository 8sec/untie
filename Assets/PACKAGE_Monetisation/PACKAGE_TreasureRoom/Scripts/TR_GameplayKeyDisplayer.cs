﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TR_GameplayKeyDisplayer : MonoBehaviour
{
    [SerializeField] private Text _currentKeyRecoltedText;
    [SerializeField] private Image[] _keyIcons;

    [SerializeField] private Sprite _enabledColor;
    [SerializeField] private Sprite _disabledColor;

    public void UpdateKeyDisplay(int currentKeyNumber)
    {
       // _currentKeyRecoltedText.text = currentKeyNumber + " / " + TR_KeyManager.Instance.MaxKeyNumber;

        for (int i = 0; i < _keyIcons.Length; i++)
        {
            if (i < currentKeyNumber)
            {
                _keyIcons[i].color = Color.white;
                _keyIcons[i].sprite = _enabledColor;
            }

            else
            {
                _keyIcons[i].color = Color.grey;
                _keyIcons[i].sprite = _disabledColor;
            }
        }
    }


}
