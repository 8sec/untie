﻿using UnityEngine;
using System.Collections;
using DG.Tweening;

public class ObjectDragger : MonoBehaviour
{
	private bool m_Moving = false;
	public bool Moving
	{
		get
		{
			return m_Moving;
		}
	}
	private Vector3 screenPoint;
	private Vector3 offset;
	public float HeightOnDrag = 0.5f;
	Vector3 targetPosition;
	public float MovementSpeed = 1f;
    private bool _isDisabled;
    public bool hasWeight => _isDisabled;
    private bool _isGroundUnder;
    public GameObject _lockObject;
    public GameObject _handleWeight;


    private GameObject _weight;

    private Vector3 _lastValidPos;
    [SerializeField] private LayerMask _groundLayer;
	private void Update()
	{
		if (m_Moving && !_isDisabled)
		{
			//transform.position = targetPosition;
			transform.position = Vector3.MoveTowards(transform.position, targetPosition, MovementSpeed * Time.deltaTime);
		}
	}
	void OnMouseDown()
	{
        if (_isDisabled)
        {
            transform.DOShakePosition(0.3f, 0.1f, 100);
            return;
        }


		RopeManager.Instance.CanWin = true;
		m_Moving = true;
		//transform.position += Vector3.up *HeightOnDrag;
		LeanTween.cancel(gameObject);
		LeanTween.move(gameObject, transform.position + Vector3.up * HeightOnDrag, 0.15f);
		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);
		offset = gameObject.transform.position + Vector3.up * HeightOnDrag - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
		VibrationManager.VibrateLight();
	}

	void OnMouseDrag()
	{
        if (_isDisabled)
        {
            return;
        }
		Vector3 curScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
		//transform.position = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;
		targetPosition = Camera.main.ScreenToWorldPoint(curScreenPoint) + offset;

        GetValidPosition();

      
	}
	private void OnMouseUp()
	{
        if (_isDisabled)
        {
            return;
        }
		m_Moving = false;
		VibrationManager.VibrateLight();
		//	Debug.LogError("up");
		LeanTween.cancel(gameObject);

        if (_isGroundUnder == false)
        {
            LeanTween.move(gameObject, _lastValidPos + Vector3.up * 0.1f, 0.15f);
        }
     //   RopeManager.Instance.
		//LeanTween.move(gameObject, transform.position - Vector3.up * HeightOnDrag, 0.15f);
	}

    public void Disable()
    {
        if (_isDisabled)
        {
          //  _lockObject.SetActive(false);
            _isDisabled = false;
            Destroy(_weight);
            transform.GetChild(0).GetComponent<MeshRenderer>().enabled = true;
            return;
        }

       // _lockObject.SetActive(true);
        _isDisabled = true;
        _weight = Instantiate(_handleWeight, transform.position, Quaternion.identity);
        _weight.GetComponent<MeshRenderer>().material.color = Color.black;
        _weight.transform.position = transform.position;
        _weight.transform.parent = transform;
       // transform.GetChild(0).GetComponent<MeshRenderer>().enabled = false;
    }

    private void GetValidPosition()
    {
        if (_isDisabled)
        {
            return;
        }
        RaycastHit hit;

        if (Physics.Raycast(transform.position, -Vector3.up, out hit, Mathf.Infinity, _groundLayer))
        {
            _lastValidPos = hit.point;
            _isGroundUnder = true;
        }

        else
        {
            _isGroundUnder = false;
        }
    }

    private void OnTriggerEnter(Collider other)
    {
        PickableCoin coin;

        if (coin = other.GetComponent<PickableCoin>())
        {
            coin.TakeCoin();
        }

        TR_Key key;

        if (key = other.GetComponent<TR_Key>())
        {
            key.RecoltKey();
        }
    }


}

