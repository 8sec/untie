﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RB_RewardMultiplier : RewardedButton
{
    public override void CollectReward()
    {
        CoinManager.Instance.TripleCurrentCoins();
    }
}
