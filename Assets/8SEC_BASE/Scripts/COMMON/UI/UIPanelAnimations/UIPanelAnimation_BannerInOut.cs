﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIPanelAnimation_BannerInOut : UIPanelAnimation
{

    public GameObject BannerGO;
    public LeanTweenType ShowAnimationEase = LeanTweenType.easeOutBack;
    public LeanTweenType HideAnimationEase = LeanTweenType.easeInBack;
    private int ltid = -1;
    public override void ShowAnimation(float animDuration)
    {
        base.ShowAnimation(animDuration);
        LeanTween.cancel(ltid);
        BannerGO.transform.localPosition += Vector3.right * -3000F;
       ltid = LeanTween.moveLocalX(BannerGO, 0f, animDuration).setEase(ShowAnimationEase).id;
    }

    public override void HideAnimation(float animDuration)
    {
        base.HideAnimation(animDuration);
        LeanTween.cancel(ltid);
        ltid = LeanTween.moveLocalX(BannerGO, 3000f, animDuration).setEase(HideAnimationEase).id;
    }
}
