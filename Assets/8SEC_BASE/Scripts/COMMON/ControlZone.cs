﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class ControlZone : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler, IPointerDownHandler, IPointerUpHandler {


    public delegate void PositionInputHandler(Vector2 inputPosition);
    public delegate void DragInputHandler(Vector2 direction, float force);


    public static event PositionInputHandler OnPointerDownEvent;
    public static event PositionInputHandler OnPointerUpEvent;
    public static event DragInputHandler OnBeginDragEvent;
    public static event DragInputHandler OnDragEvent;
    public static event DragInputHandler OnEndDragEvent;
    

    public UnityEngine.UI.Text DEBUG_DragValue;
    public float AngleOffset = 0f;

    [Header ("On-screen Joystick")]
    public bool TWO_STEP_JOYSTICK = false;
    public float TwoStepRatio = 0.5f;
    public RectTransform UI_Joystick;
    public RectTransform UI_JoystickHead;

    public float FingerTrackerZDistance;

    Vector2 joystickBeginPosition;

   
    private Vector2 beginDragPosition;
    private Vector2 currentDrag;
    private float currentDragRatio;

    public Vector2 MaxPixelAmplitude;

    public void OnPointerDown (PointerEventData eventData) {

        if (TWO_STEP_JOYSTICK) {

        }
        else {
          

        }

        if (UI_Joystick != null) {
            UI_Joystick.gameObject.SetActive (true);

            UI_Joystick.anchoredPosition = eventData.pressPosition;

        }

        if (FingerTracker.Instance != null) {
            Vector3 p = Camera.main.ScreenToWorldPoint (new Vector3 (eventData.position.x, eventData.position.y, FingerTrackerZDistance));

            lastWorldPosition = p;
            //   FingerTracker.gameObject.SetActive(true);
            FingerTracker.Instance.ShowAt(p);
        }

        if (OnPointerDownEvent != null)
            OnPointerDownEvent(eventData.position);
    }

    public void OnBeginDrag (PointerEventData eventData) {
        beginDragPosition = eventData.position;
        joystickBeginPosition = eventData.pressPosition;

        Vector3 p = Camera.main.ScreenToWorldPoint (new Vector3 (eventData.position.x, eventData.position.y, FingerTrackerZDistance));

        if (FingerTracker.Instance != null) {

            lastWorldPosition = p;
            FingerTracker.Instance.ShowAt(p);
        }

        if (OnBeginDragEvent != null)
            OnBeginDragEvent(currentDrag.normalized,currentDragRatio);

    }
    Vector3 lastWorldPosition;
    public void OnDrag (PointerEventData eventData) {
        Vector3 p = Camera.main.ScreenToWorldPoint (new Vector3 (eventData.position.x, eventData.position.y, 10f));

        currentDrag = beginDragPosition - eventData.position;
        currentDrag.x = Mathf.Clamp (currentDrag.x / MaxPixelAmplitude.x, -1f, 1f);
        currentDrag.y = Mathf.Clamp (currentDrag.y / MaxPixelAmplitude.y, -1f, 1f);
        if (DEBUG_DragValue != null) DEBUG_DragValue.text = currentDrag.x.ToString ("0.00") + " " + currentDrag.y.ToString ("0.00");

       
        Vector2 pData = eventData.position;
        float s = Mathf.Sin (Mathf.Deg2Rad * AngleOffset);
        float c = Mathf.Cos (Mathf.Deg2Rad * AngleOffset);

        // translate point back to origin:
        pData.x -= beginDragPosition.x;
        pData.y -= beginDragPosition.y;

        // rotate point
        float xnew = pData.x * c - pData.y * s;
        float ynew = pData.x * s + pData.y * c;

        // translate point back:
        pData.x = xnew + beginDragPosition.x;
        pData.y = ynew + beginDragPosition.y;

        currentDrag = beginDragPosition - pData; 
        currentDragRatio = currentDrag.magnitude / MaxPixelAmplitude.magnitude;
        currentDragRatio = Mathf.Clamp01 (currentDragRatio);
     
        if (DEBUG_DragValue != null) DEBUG_DragValue.text = currentDrag.x.ToString ("0.00") + " " + currentDrag.y.ToString ("0.00");

        if (UI_Joystick != null) {
            Vector2 v = eventData.position - joystickBeginPosition;

            if (UI_JoystickHead) {
                Vector3 t = (eventData.position - eventData.pressPosition);

                UI_JoystickHead.anchoredPosition = v.normalized * Mathf.Min (75f, t.magnitude);

            }
        }
      

        if (TWO_STEP_JOYSTICK) {

        }
        Vector3 worldPosition = Camera.main.ScreenToWorldPoint (new Vector3 (eventData.position.x, eventData.position.y, FingerTrackerZDistance));

        if (FingerTracker.Instance != null) {

            FingerTracker.Instance.ShowAt (worldPosition);

        }

        if (Vector2.Distance (lastWorldPosition, worldPosition) > 0.5f)
            lastWorldPosition = worldPosition;

        if (OnDragEvent != null)
            OnDragEvent(currentDrag.normalized,currentDragRatio);
    }

    public void OnEndDrag (PointerEventData eventData) {


        if (OnEndDragEvent != null)
            OnEndDragEvent(currentDrag.normalized , currentDragRatio);

    }

    public void OnPointerUp (PointerEventData eventData) {
    
        if (UI_Joystick != null) {
            UI_Joystick.gameObject.SetActive (false);
        }

        if (FingerTracker.Instance != null)
            FingerTracker.Instance.Hide ();

        if (OnPointerUpEvent != null)
            OnPointerUpEvent(eventData.position);
    }

}