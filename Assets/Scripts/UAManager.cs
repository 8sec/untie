﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class UAManager : MonoBehaviour
{
    public static UAManager Instance;
    public bool hideUiOnStart;
    public bool overrideLevelTheme;
    //public LevelTheme overridingLevelTheme;

    public List<CanvasGroup> uiElementsToHide;
    public List<GameObject> framesImages;
    public List<Graphic> uiElements;

    private void Awake()
    {
        Instance = this;

        Debug.LogError("WARNING : UA MANAGER IN SCENE");

    }

    // Start is called before the first frame update
    void Start()
    {
        if(framesImages.Count > 0)
        {
            foreach (var frameImage in framesImages)
            {
                frameImage.SetActive(false);
            }
        }


        if (hideUiOnStart)
        {
            HideUI();
        }

        if (overrideLevelTheme)
        {
            OverrideLevelTheme();
        }

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.H))
        {
            HideUI();
            OverrideLevelTheme();
        }
    }

    public void HideUI()
    {

        uiElements.AddRange(FindObjectsOfType<Graphic>());

        foreach (var uiElement in uiElements)
        {
            if(uiElement.gameObject.name != "Control" && uiElement.GetComponent<TextMeshPro>() == null)
            {
                uiElement.enabled = false;
            }

        }
    }

    public void OverrideLevelTheme()
    {
        //LevelManager.Instance.CurrentTheme = overridingLevelTheme;
        //LevelManager.Instance.ApplyTheme(overridingLevelTheme);
    }

   /* [ContextMenu("Unlock Shop")]
    void UnlockEverything()
    {
        foreach (var ag in CustomizationManager.Instance.AvailableGroups)
        {
            //foreach (var customItem in ag.AvailableItems)
            //{
            //    customItem.Unlock();
            //}
        }
    }*/
}
