﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HelpPannel : MonoBehaviour
{
    bool _disabled;
    // Start is called before the first frame update
    void Start()
    {
        LeanTween.scale(gameObject, Vector3.one * 0.72f, 0.5f).setLoopPingPong(-1);
    }

    // Update is called once per frame
    void Update()
    {
        if (gameObject.activeSelf && Input.GetMouseButtonDown(0) && !_disabled)
        {
            LeanTween.cancel(gameObject);
            _disabled = true;
            LeanTween.scale(gameObject, Vector3.zero, 0.2f).setEaseInBack();
        }
    }
}
