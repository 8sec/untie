﻿using System.Collections;
using System.Collections.Generic;
using Obi;
using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(Obi.ObiRope))]
public class Rope : MonoBehaviour
{
    [SerializeField] private bool _isPreviewRope;
    private ObiRope m_Rope;

    public Transform AnchorA;
    public Transform AnchorB;

    public ObjectDragger DraggableAnchorA;
    public ObjectDragger DraggableAnchorB;

    public ObiParticleAttachment AttachmentA;
    public ObiParticleAttachment AttachmentB;

    bool canShrink;

    public bool IsPreviewRope => _isPreviewRope;

    public bool Moving
    {

        get
        {
            return DraggableAnchorA.Moving || DraggableAnchorB.Moving;
        }
    }
    private void Awake()
    {
        m_Rope = GetComponent<ObiRope>();
    }

 
    void Start()
    {
        if (_isPreviewRope == false)
        {
            Color c = Random.ColorHSV();
            c.a = 1f;
            SetRopeColor(RopeManager.Instance.GetRopeColor());
        }
        //   SetAnchorsOnRopeEnds();

    }

    // Update is called once per frame
    void Update()
    {
        
        if (cursor != null)
        {
            cursor.ChangeLength(m_Rope.restLength - Time.deltaTime* cursorSpeed);
        }

        if (Input.GetKeyDown(KeyCode.D))
            DetachAnchors();
    }

    [Button("Detach anchors")]
    public void DetachAnchors()
    {
        AttachmentA.enabled = false;
        AttachmentA.target = null;
        AttachmentB.enabled = false;
        AttachmentB.target = null;
    }

    [Button("WEIGHT ON B ANCHOR")]
    public void WeightOnB()
    {
        SetUnmovable(false, AnchorPoint.B);
    }

    [Button("WEIGHT ON A ANCHOR")]
    public void WeightOnA()
    {
        SetUnmovable(false, AnchorPoint.A);
    }

    public void SetUnmovable(bool isMovable, AnchorPoint unmovableAnchor)
    {
        if (isMovable == false)
        {
            switch (unmovableAnchor)
            {
                case AnchorPoint.A:
                    DraggableAnchorA.Disable();
                    break;
                case AnchorPoint.B:
                    DraggableAnchorB.Disable();
                    break;
                default:
                    break;
            }
        }

        else
        {
            switch (unmovableAnchor)
            {
                case AnchorPoint.A:
                    DraggableAnchorA.enabled = true;
                    break;
                case AnchorPoint.B:
                    DraggableAnchorB.enabled = true;
                    break;
                default:
                    break;
            }
        }
    }

    public void AttachAnchors()
    {
        Time.timeScale = 0f;
        ObiRope rope = GetComponent<ObiRope>();

        var pa = AttachmentA.particleGroup;
     //   AttachmentA = gameObject.AddComponent<ObiParticleAttachment>();
        
        AttachmentA.target = AnchorA;
        AttachmentA.enabled = true;
        AttachmentA.particleGroup = pa;
        
       

        AttachmentB.target = AnchorB;
        AttachmentB.enabled = true;
        Time.timeScale = 1f;

    }



    public int DEBUG_index = 0;
    [Button("Set Anchors Position")]
    public void SetAnchorsOnRopeEnds()
    {
        /*
        // solver index of the first particle in the actor.
        int solverIndex = m_Rope.solverIndices[0];

        // use it to get the particle's current velocity.
        Vector3 p = m_Rope.solver.positions[solverIndex];
        */


        DetachAnchors();
        
        ObiRope rope = GetComponent<ObiRope>();


        Debug.Log("Setting positions");

        Vector3 pStartLocal = rope.path.points[0].position;
        Vector3 pStartWorld = transform.TransformPoint(pStartLocal);

        pStartWorld = rope.solver.positions[rope.solverIndices[0]];
       // pStartWorld = transform.TransformPoint(pStartWorld);
        if (AnchorA)
            AnchorA.transform.position = pStartWorld;

        //Debug.LogError("settings positons");
        
        //Debug.Log("Solver indices : " + rope.solverIndices.Length);
        int indexLast = rope.blueprint.activeParticleCount-1;
        //Debug.Log("Active index : " + rope.activeParticleCount);

        Vector3 pEndWorld = rope.solver.positions[rope.solverIndices[indexLast]];
      //  pEndWorld = transform.TransformPoint(pEndWorld);

        if (AnchorB)
            AnchorB.transform.position = pEndWorld;// GetComponent<ObiRope>().GetParticlePosition(GetComponent<ObiRope>().particleCount - 1);


        
        AttachAnchors();
       
    }

    private ObiRopeCursor cursor;
    float cursorSpeed;
    public float SuccessAnimationDuration = 1f;

    [Button("Success anim")]
    public void SuccessAnimation(float delay = 0)
    {
        Debug.LogError("ACTUALLY PLAYING SUCESS ANIM");
        Vector3 dest;

        dest = (AnchorA.position + AnchorB.position)*0.5f;

        LeanTween.move(AnchorA.gameObject, dest, SuccessAnimationDuration).setDelay(delay);
        LeanTween.move(AnchorB.gameObject, dest, SuccessAnimationDuration).setDelay(delay).setOnComplete(playParticles) ;

        cursor = gameObject.AddComponent<ObiRopeCursor>();
        cursorSpeed = m_Rope.restLength/ SuccessAnimationDuration;

    }

   

    public ParticleSystem SuccessParticles;
    void playParticles()
    {
        if(SuccessParticles!=null)
        {
            AnchorA.gameObject.SetActive(false);
            AnchorB.gameObject.SetActive(false);
            SuccessParticles.transform.position = AnchorA.transform.position;
            SuccessParticles.Play();
            VibrationManager.VibrateHeavy();
        }
    }
    public void SetRopeState(bool state)
    {
        gameObject.SetActive(state);
        AnchorA.gameObject.SetActive(state);
        AnchorB.gameObject.SetActive(state);
    }

    public void PrintParticleIndices()
    {
        string s = name+" : ";
        for (int i = 0; i < m_Rope.solverIndices.Length; i++)
        {
            s += " " + m_Rope.solverIndices[i].ToString();
        }
        Debug.Log(s);
    }
  

    public void SetRopeColor(Color c)
    {
        GetComponent<MeshRenderer>().material.color = c*0.8f;
        AnchorA.GetComponentInChildren<MeshRenderer>().material.color = c;
        AnchorB.GetComponentInChildren<MeshRenderer>().material.color = c;
        ShopManager.Instance.UpdateShop();
    }
}

public enum AnchorPoint
{
    A,
    B
}
