﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ToggleVibration : MonoBehaviour {
    public Image ButtonImage;
    public Sprite ON;
    public Sprite OFF;

    public void Toggle () {
        UserConfig.SetVibration (!UserConfig.Vibration);

        if (ButtonImage == null || ON == null || OFF == null) {
            Debug.LogError ("ToggleVibration : One or more public references aren't set. ");
            return;
        }

        if (UserConfig.Vibration) {
            ButtonImage.sprite = ON;
            VibrationManager.VibrateMedium ();
        } else
            ButtonImage.sprite = OFF;
    }
}