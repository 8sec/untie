﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine;
using Sirenix.Serialization;
using System;
using System.Text.RegularExpressions;

public class ItemCategory : MonoBehaviour
{
    public delegate void OnCategoryInitialized();
    public static event OnCategoryInitialized OnCategoryInitializedEvent;

    [SerializeField] private ShopCategoryItem _itemCategory;
    [SerializeField] private List<ShopItem> _items = new List<ShopItem>();

    private ShopItem _currentlySelectedItem;

    public ShopCategoryItem ShopItemCategory => _itemCategory;
    public List<ShopItem> Items => _items;
    public ShopItem CurrentlySelectedItem => _currentlySelectedItem;


    private string _pathSavedItem;
    public string PathSavedItem => _pathSavedItem;

    private void Awake()
    {
        ShopManager.OnShopExitEvent += SelectAlreadyEquippedItem;
    }

    private void OnDestroy()
    {
        ShopManager.OnShopExitEvent -= SelectAlreadyEquippedItem;
    }


    public void Init()
    {
        for (int i = 0; i < _items.Count; i++)
        {
            _items[i].SetShopItemCategory(this);
        }

        SelectAlreadyEquippedItem();
        _currentlySelectedItem.Select();
        OnCategoryInitializedEvent?.Invoke();
        gameObject.SetActive(false);
    }

    public void DeletePrefs()
    {
        _pathSavedItem = _itemCategory.ToString();
        if (File.Exists(_pathSavedItem))
        {
            File.Delete(_pathSavedItem);
            Debug.LogError("Prefs of the shop category " + _itemCategory + " deleted successfully");
        }
        else
        {
            Debug.LogError("There was no prefs saved for shop category : " + _itemCategory);
        }
    }

    public void Select(ShopItem selectedItem)
    {
        for (int i = 0; i < _items.Count; i++)
        {
            if (_items[i] != selectedItem)
            {
                _items[i].Deselect();
            }
        }

        _currentlySelectedItem = selectedItem;
        if (_currentlySelectedItem.IsUnlocked)
        {
            ShopManager.Instance.SaveDictionnary[_itemCategory] = selectedItem.UniqueID;
            string s = "";

            foreach (var item in ShopManager.Instance.SaveDictionnary)
            {
                s += JsonUtility.ToJson(new DictionaryToJson(item.Key, item.Value));
            }

            TextWriter writer;

            if (File.Exists(_pathSavedItem))
            {
                File.Delete(_pathSavedItem);
            }

            writer = File.AppendText(_pathSavedItem);
            writer.Write(s);
            writer.Close();
        }

        ShopManager.Instance.UpdateShop();
    }

    private void SelectAlreadyEquippedItem()
    {
        Debug.LogError("SELECTING ALREADY EQUIPPED");
        _pathSavedItem = _itemCategory.ToString();

        if (File.Exists(_pathSavedItem) == false)
        {
            ShopItem unlockedByDefault = _items.Find(item => item._isUnlockByDefault == true);
            
            if (unlockedByDefault == null)
            {
                Debug.LogError("YOU DID NOT SET WHICH ITEM SHOULD BE SELECTED BY DEFAULT IN THE ITEMS OF CATERGORY " + _itemCategory);
            }

            _currentlySelectedItem = unlockedByDefault;
            ShopManager.Instance.UpdateShop();
            Debug.LogError("Returning");
            return;
        }

       
     
        string dataAsJson = File.ReadAllText(_pathSavedItem);


        var lines = dataAsJson.Split(
            new[] { "{", "}" },
            StringSplitOptions.RemoveEmptyEntries
            );


        foreach (var l in lines)
        {
            var splitLine = l.Split(',');
            string category = splitLine[0];
            string savedItemID = splitLine[1];

            string s = Regex.Match(category, @"\d+").Value;
            int categoryIndex;
            int.TryParse(s, out categoryIndex);
            ShopCategoryItem itemCategory = (ShopCategoryItem)categoryIndex;
         
            if (itemCategory == _itemCategory)
            {

                savedItemID = savedItemID.Replace("\"", "");

                int index = 0;
                for (int i = 0; i < savedItemID.Length; i++)
                {
                    if (savedItemID[i] == ':')
                    {
                        index = i + 1;
                        break;
                    }
                }

                savedItemID = savedItemID.Substring(index);
                Debug.LogError(_itemCategory + " SAVED ITEM ID : " + savedItemID);

                for (int i = 0; i < _items.Count; i++)
                {
                    if (_items[i].UniqueID == savedItemID)
                    {
                        ShopItem itemToSelect = _items[i];
                        _currentlySelectedItem = itemToSelect;
                        ShopManager.Instance.UpdateShop();
                  //      Debug.LogError(_itemCategory + " " + _currentlySelectedItem);
                        break;
                    }
                }

            }
        }
    
    }
}
