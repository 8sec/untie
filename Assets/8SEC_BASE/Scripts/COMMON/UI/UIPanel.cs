﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent (typeof (CanvasGroup))]
public class UIPanel : MonoBehaviour {

    public UIPanelName PanelName = (UIPanelName) (-1);
    public bool BlurBackground = false;
    protected CanvasGroup m_PanelCanvasGroup;
    public CanvasGroup PanelCanvasGroup {
        get {
            if (m_PanelCanvasGroup == null)
                m_PanelCanvasGroup = GetComponent<CanvasGroup> ();
            return m_PanelCanvasGroup;
        }

    }

    List<UIPanelAnimation> panelAnimations = new List<UIPanelAnimation> ();
    void Awake () {
        panelAnimations = new List<UIPanelAnimation>(GetComponents<UIPanelAnimation> ());
    }

    private int ltid = -1;

    public void ShowInstantly () {
        gameObject.SetActive (true);

        LeanTween.cancel (ltid);
        PanelCanvasGroup.interactable = true;
        PanelCanvasGroup.blocksRaycasts = true;
        PanelCanvasGroup.alpha = 1f;
        foreach (var anim in panelAnimations)
        {
            if (anim != null)

                anim.ShowAnimation(0f);
        }
    }

    public virtual void Show (float animDuration) {
        gameObject.SetActive (true);

        LeanTween.cancel (ltid);
        PanelCanvasGroup.interactable = true;
        PanelCanvasGroup.blocksRaycasts = true;

        ltid = LeanTween.alphaCanvas (PanelCanvasGroup, 1f, animDuration).setEase (UIController.Instance.AnimationEase).setIgnoreTimeScale (true).id;

        foreach (var anim in panelAnimations)
        {
            if (anim != null)

                anim.ShowAnimation(animDuration);
        }
     
    }

    public void HideInstantly () {
        LeanTween.cancel (ltid);

        PanelCanvasGroup.alpha = 0f;
        PanelCanvasGroup.interactable = false;
        PanelCanvasGroup.blocksRaycasts = false;
        gameObject.SetActive (false);
        foreach (var anim in panelAnimations)
        {
            if (anim != null)

                anim.HideAnimation(0f);
        }
    }

    public void Hide (float animDuration) {
        LeanTween.cancel (ltid);
        PanelCanvasGroup.interactable = false;
        PanelCanvasGroup.blocksRaycasts = false;

        ltid = LeanTween.alphaCanvas (PanelCanvasGroup, 0f, UIController.Instance.AnimationDuration).setEase (UIController.Instance.AnimationEase).setIgnoreTimeScale (true).setOnComplete (n => {
            gameObject.SetActive (false);

        }).id;
        foreach (var anim in panelAnimations)
        {
            if(anim!=null)
            anim.HideAnimation(animDuration);
        }

    }
}