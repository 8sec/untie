﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScollMaterial : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }
    public Material toScroll;
    private float offset;
    // Update is called once per frame
    void Update()
    {
        offset += Time.deltaTime;
        if (toScroll != null)
        {
            toScroll.SetTextureOffset("_MainTex", new Vector2(0, offset));
        }

    }
}
