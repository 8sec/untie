﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class ShopItem_Spawnable : ShopItem
{
    [SerializeField] private bool _isMystery;
    [SerializeField] private GameObject _objectToSpawn;
    [SerializeField] private Transform _previewParent;
    [SerializeField] private GameObject _noItemImage;

    public GameObject ObjectToSpawn => _objectToSpawn;
    public bool IsMystery => _isMystery;

    public override void Init()
    {
        base.Init();

        if (_isMystery == true)
        {
            gameObject.SetActive(false);
        }

        if (_objectToSpawn != null)
        {
            GameObject go = Instantiate(_objectToSpawn, _previewParent.position, _previewParent.rotation, _previewParent);
            go.transform.localScale = Vector3.one;
            go.layer = LayerMask.NameToLayer("UI");
        }

        else
        {
            _noItemImage.SetActive(true);
            Debug.LogError("Object to spawn is not set, it's ok if it's the no skin item");
        }
       
    }

}
