﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;
public class ParticleGizmo : MonoBehaviour
{
    public ObiRope Rope;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        transform.position = Rope.solver.positions[Rope.solverIndices[0]];
    }
}
