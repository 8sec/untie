﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent (typeof (Slider))]
public class LevelProgressSliderDisplayer : MonoBehaviour {

    protected Slider m_Slider;
    protected void Awake () {
        m_Slider = GetComponent<Slider> ();
    }

    // Start is called before the first frame update
    protected void Start () {
        InitSliderMinMaxValues ();
    }

    // Update is called once per frame
    protected void Update () {
        updateProgress ();
    }

    public virtual void InitSliderMinMaxValues () {
        m_Slider.minValue = 0f;
        m_Slider.maxValue = 100f;
    }

    public virtual void updateProgress () {
        // SET PROGRESS HERE
        m_Slider.value = GameManager.Instance.CurrentScore;
    }
}