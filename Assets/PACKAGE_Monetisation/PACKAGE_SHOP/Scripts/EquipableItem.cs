﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

public class EquipableItem : ShopItem
{
    public Transform EquipableItemPreviewParent;
    public GameObject EquipableItemGO;


    private void Start()
    {
        Debug.LogError(" AT START : " + EquipableItemGO);
    }

  
  // public override void ApplyCustomItem()
  // {
  //     Debug.LogError("Applying custom item");
  //     if (IsUnlocked)
  //     {
  //         if (PlayerEquipables.Instance != null)
  //         {
  //             PlayerEquipables.Instance.SetEquipable(EquipableItemGO);
  //         }
  //
  //         else
  //         {
  //             Debug.LogError("PLAYER EQUIPABLE IS NULL");
  //         }
  //     }
  // }

    public override void Select()
    {
        Debug.LogError(EquipableItemGO);
       // ShopManager.Instance.DeselectAll();
        base.Select();
        if (ShopPreviewDisplayer.Instance != null)
        {
            ShopPreviewDisplayer.Instance.DisplayEquipableItem(EquipableItemGO, IsUnlocked);
        }
    }

    public override void Init()
    {
        base.Init();
        SpawnPreview();
        UpdateLockState();
    }

    public override void TryEquip()
    {
        base.TryEquip();
       // ShopManager.Instance.SetEquipableItem(this);
    }

    public override void Unlock()
    {
        base.Unlock();
        foreach (Transform t in EquipableItemPreviewParent)
        {
            Destroy(t.gameObject);
        }

        SpawnPreview();
    }
    
    private void SpawnPreview()
    {
        var c = Instantiate(EquipableItemGO, EquipableItemPreviewParent);
        c.SetActive(true);
        c.transform.localPosition = Vector3.zero;
        c.transform.localEulerAngles = Vector3.zero;
        c.transform.localScale = Vector3.one;
    }

    public void UpdateLockState()
    {
      //  if (ShopManager.Instance.DisableMaterialForLockedItems == false)
      //  {
      //      return;
      //  }

        if (!IsUnlocked)
        {
            foreach(var mr in GetComponentsInChildren<MeshRenderer>())
            {
                for (int i = 0; i < mr.materials.Length; i++)
                {
                    mr.material = null;
                }
            }

            foreach (var mr in GetComponentsInChildren<SkinnedMeshRenderer>())
            {
                for (int i = 0; i < mr.materials.Length; i++)
                {
                    mr.material = null;
                }
            }
        }
    }
}
