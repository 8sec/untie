﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;
using UnityEngine.UI;
using Sirenix.OdinInspector;
using UnityEditor;
using System;

public class RopeManager : MonoBehaviour
{
	public static RopeManager Instance;
	public Text GameStateDisplayer;

	public ObiSolver solver;
	
	public Rope RopeAscript;
	public Rope RopeBscript;

	public List<ObiRope> RopeActors = new List<ObiRope>();
	Obi.ObiSolver.ObiCollisionEventArgs collisionEvent;

	[Header("Game Settings")]
	public float DurationForWin = 1f;
	public float DelayBetweenSuccessAnimations = 0.2F;
	public bool DEBUG_LockWin = false;
    bool obstacleDisabled;

    private float timer;

    bool colWithObstacle;

	void Awake()
	{
		Instance = this;
		Application.targetFrameRate = 60;
#if !UNITY_EDITOR
		 DEBUG_LockWin = false;
#endif
		CanWin = false;
		//solver = GetComponent<Obi.ObiSolver>();
	}
	private void Start()
	{
		Camera.main.transform.position -= Vector3.right * 10f;
		LeanTween.moveX(Camera.main.gameObject, 0f, 0.5f).setEase(LeanTweenType.easeOutCubic);

		GameStateDisplayer.text = "Resting Ropes";


	}
	void OnEnable()
	{
		solver.OnParticleCollision += Solver_OnParticleCollisionWithParticle;
        solver.OnCollision += Solver_OnParticleCollisionWithOther;
	}

    private void Solver_OnParticleCollisionWithOther(ObiSolver solver, ObiSolver.ObiCollisionEventArgs frame)
    {
        bool hasCollided = false;
        for (int i = 0; i < frame.contacts.Count; i++)
        {
            Component col = ObiCollider.idToCollider[frame.contacts[i].other];
            if (col.CompareTag("Obstacle"))
            {
                col.GetComponent<Obstacle>().OnCollision();
                colWithObstacle = true;
                hasCollided = true;
                obstacleDisabled = false;
            }


            else if (col.CompareTag("Coin"))
            {
                bool canPickup = false;

                for (int j = 0; j < RopeActors.Count; j++)
                {
                    if (RopeActors[j].gameObject.activeSelf)
                    {
                        if (RopeActors[j].GetComponent<Rope>().Moving == true)
                        {
                            canPickup = true;
                            break;
                        }
                    }
                }

                if (canPickup)
                {
                    col.GetComponent<PickableCoin>().TakeCoin();
                }
            }

            else
            {
                if (obstacleDisabled == false && timer > 0.5f)
                {
                    Obstacle[] obstacles = FindObjectsOfType<Obstacle>();

                    if (obstacles.Length > 0)
                    {
                        for (int d = 0; d < obstacles.Length; d++)
                        {
                            obstacles[d].OnNoCollision();
                        }

                        timer = 0;

                    }

                    obstacleDisabled = true;
                }
            }
        }

        if (hasCollided == false)
        {
            colWithObstacle = false;
        }

    }

    void OnDisable()
	{
		solver.OnParticleCollision -= Solver_OnParticleCollisionWithParticle;
        solver.OnCollision -= Solver_OnParticleCollisionWithOther;

        // solver.OnCollision -= ooo;
    }

    List<ObiRope> InContact = new List<ObiRope>();
	List<ObiRope> RopesInPlay = new List<ObiRope>();

	int eventCollisionCount = 0;
    void Solver_OnParticleCollisionWithParticle(object sender, Obi.ObiSolver.ObiCollisionEventArgs frame)
    {
        bool hadContact = false;
        int ropesInContact = 0;
        eventCollisionCount++;
        //Debug.LogError("Event number : " + eventCollisionCount);
        foreach (Oni.Contact contact in frame.contacts)
        {
            InContact.Clear();
            ropesInContact = 0;
            bool hasContacts = false;
            if (contact.distance < 0.01)
            {
                foreach (var r in RopeActors)
                {
                    if (r.gameObject.activeSelf)
                    {

                        for (int i = 0; i < r.solverIndices.Length; i++)
                        {
                            if (r.solverIndices[i] == contact.particle
                                || r.solverIndices[i] == contact.other)
                            {
                                if (!InContact.Contains(r))
                                    InContact.Add(r);
                                ropesInContact++;
                                break;
                            }

                        }

                      
                    }

                }

                if (ropesInContact > 0 && InContact.Count > 1)
                {
                    hadContact = true;
                    CancelWin();
                }


            }


        }

        if (!hadContact && colWithObstacle == false)
        {
            if (RopeAscript.Moving || RopeBscript.Moving)
                CancelWin();
            else
                TryWin();

        }


      // for (int i = 0; i < RopeActors.Count; i++)
      // {
      //     if (RopeActors[i].gameObject.activeSelf)
      //     {
      //         if (InContact.Contains(RopeActors[i]))
      //         {
      //             RopeActors[i].GetComponent<Rope>().CancelSuccessAnim();
      //         }
      //
      //         else
      //         {
      //
      //             RopeActors[i].GetComponent<Rope>().TriggerSuccessAnim();
      //         }
      //     }
      // }
    }

	private void Update()
	{
		if (RopeAscript.Moving || RopeBscript.Moving)
			CancelWin();

        timer += Time.deltaTime;

	}

	public void CancelWin()
	{
		CancelInvoke("Win");
		GameStateDisplayer.text = "Moving / Resting Ropes";


	}
	public bool CanWin = false;
	public void TryWin()
	{
		//CancelWin();

		if(CanWin)
			Invoke("Win", DurationForWin);

	}



	bool won = false;
	public ParticleSystem SuccessParticles;
	public void Win()
	{
		if (won || DEBUG_LockWin)
			return;
		if (SuccessParticles != null)
			SuccessParticles.Play();
		won = true;
		CancelInvoke("Win");

		GameStateDisplayer.text = "WIN";
		Debug.LogError("WIN detected");
		GameManager.Instance.LevelSuccess();
		for (int i = 0; i < RopeActors.Count; i++)
		{
			if(RopeActors[i].gameObject.activeSelf)
				RopeActors[i].GetComponent<Rope>().SuccessAnimation(i*DelayBetweenSuccessAnimations);
    
		}

	}

	public List<Color> AvailableRopeColors = new List<Color>();

	private List<Color> GivenColors = new List<Color>();

	public Color GetRopeColor()
	{
		if (AvailableRopeColors.Count == 0)
		{
			Debug.LogError("No more available colors. Returning random color");
			Color c = new Color(UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), UnityEngine.Random.Range(0f, 1f), 1f);
			return c;		
		}

		Color res = AvailableRopeColors[UnityEngine.Random.Range(0, AvailableRopeColors.Count)];
		AvailableRopeColors.Remove(res);
		return res;

	}
}
