﻿using UnityEngine;
using System.Collections;

public class LockUIItem : MonoBehaviour
{
    private Vector3 position;
    private RectTransform rectT;
    // Use this for initialization
    void Awake()
    {
        rectT = this.gameObject.GetComponent<RectTransform>();
        position = rectT.position;
    }

    // Update is called once per frame
    void Update()
    {
        if (position != rectT.position)
        {
            rectT.position = position;
        }
    }
}
