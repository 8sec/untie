﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EquipedItemsDisplayer : MonoBehaviour
{
    [SerializeField] private MeshRenderer _meshRenderer;
    [SerializeField] private List<SpawnableDisplayer> _spawnableDisplayers = new List<SpawnableDisplayer>();
    [SerializeField] private Animator _animator;
    private string _animationWinTrigger;

    private bool _isScrollingMat;
    private float _scrollSpeed;
    float offset;

    public Material Material => _meshRenderer.material;
    // Start is called before the first frame update

    private void Awake()
    {
        ShopManager.OnShopUpdateEvent += UpdatePlayerSkin;
        ItemCategory.OnCategoryInitializedEvent += UpdatePlayerSkin;
    }

    private void OnDestroy()
    {
        ShopManager.OnShopUpdateEvent -= UpdatePlayerSkin;
        ItemCategory.OnCategoryInitializedEvent -= UpdatePlayerSkin;
    }

    

    private void UpdatePlayerSkin()
    {
        ShopItem_Material MaterialItem = (ShopItem_Material)ShopManager.Instance.GetCurrentlySelectedItem(ShopCategoryItem.Material);

        if (MaterialItem != null)
        {
            Color c = _meshRenderer.material.color;
            _isScrollingMat = MaterialItem.IsScrolling;
            _meshRenderer.material = MaterialItem.Material;
            _meshRenderer.material.color = c;
            _scrollSpeed = _isScrollingMat == true ? MaterialItem.ScrollSpeed : 0;
        }

        ShopItem_Animation AnimationItem = (ShopItem_Animation)ShopManager.Instance.GetCurrentlySelectedItem(ShopCategoryItem.ANIMATION);

        if (AnimationItem != null)
        {
            _animationWinTrigger = AnimationItem.AnimationName;
            _animator.SetTrigger(_animationWinTrigger);
        }

        for (int i = 0; i < _spawnableDisplayers.Count; i++)
        {
            _spawnableDisplayers[i].UpdateDisplay();
        }
    }

    private void Update()
    {
        if (_isScrollingMat)
        {
            offset += Time.deltaTime * _scrollSpeed;
            Vector2 currOffset = _meshRenderer.material.GetTextureOffset("_MainTex");
            _meshRenderer.material.SetTextureOffset("_MainTex", new Vector2(currOffset.x, offset));
        }
    }

}
