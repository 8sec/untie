﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShopTab : MonoBehaviour
{
    public delegate void OnTabSelected(ShopTab selectedTab);
    public static event OnTabSelected OnTabSelectedEvent;

    [SerializeField] private ShopCategoryItem _tabCategory;
    public ShopCategoryItem TabCategory => _tabCategory;
    private Image _tabImage;

    private void Awake()
    {
        _tabImage = GetComponent<Image>();
    }

    public void SetColor(Color tabColor)
    {
        if (gameObject.activeSelf)
        {
            _tabImage.color = tabColor;
        }
    }

   

    public void OnTabClicked()
    {
        OnTabSelectedEvent?.Invoke(this);
    }

    private void OnValidate()
    {
        gameObject.name = "Tab_" + _tabCategory;
    }


}
