﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
[RequireComponent (typeof (Text))]
public class ScoreDisplayer : MonoBehaviour {
	public string TextPrefix;

	public string TextSuffix;

	public float UpdateAnimationDuration = 0.2f;
	public float ScaleUpFactor = 1.1f;
	Text scoreDisplayerText;

	void Awake () {
		scoreDisplayerText = GetComponent<Text> ();
	}
	// Use this for initialization
	void Start () {
		GameManager.OnScoreUpdate += updateScore;
	}
	private void OnDestroy () {
		GameManager.OnScoreUpdate -= updateScore;

	}

	void updateScore (int n) {
		LeanTween.cancel (gameObject);
		LeanTween.scale (gameObject, Vector3.one * ScaleUpFactor, UpdateAnimationDuration * 0.5f).setOnComplete (c => {
			//scoreDisplayerText.text = ScorePrefix + n.ToString () + ScoreSuffix;
			scoreDisplayerText.text = string.Concat (TextPrefix, n.ToString (), TextSuffix);

			LeanTween.scale (gameObject, Vector3.one, UpdateAnimationDuration * 0.5f);

		});
	}

}