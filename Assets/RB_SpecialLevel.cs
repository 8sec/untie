﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RB_SpecialLevel : RewardedButton
{
    public override void CollectReward()
    {
        base.CollectReward();
        StateStoreLoad.Instance.PlaySpecialLevel();
        GetComponentInParent<PopupSpecialLevel>().HidePopup();
    }
}
