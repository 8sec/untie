﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FingerTracker : MonoBehaviour {
    public static FingerTracker Instance;
    public TrailRenderer FingerTrail;

    private void Awake () {
        Instance = this;
        Hide ();
    }
    // Start is called before the first frame update
    void Start () {

    }

    // Update is called once per frame
    void Update () {

    }

    public void Clear () {
        FingerTrail.Clear ();
    }

    bool showing = false;
    public void ShowAt (Vector3 p) {
        transform.position = p;

        if (!showing) {
            showing = true;
            Clear ();
            gameObject.SetActive (true);
        }

    }

    public void Hide () {
        showing = false;
        gameObject.SetActive (false);
        Clear ();
    }
}