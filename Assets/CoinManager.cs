﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;
using UnityEngine.UI;

public class CoinManager : MonoBehaviour
{
    public static CoinManager Instance;
    [SerializeField] private GameObject[] _coinCurves;
    [SerializeField] private ParticleSystem _coinFX;

    public int TotalCoinsThisLevel => _currentCoinThisLevel + 5;

    bool _isTripleReward;




    private const string COIN_KEY = "CoinKey";

    private int _coinAmount;

    private int _currentCurveIndex = -1;
    private int _currentCoinThisLevel;

    private void Awake()
    {
        Instance = this;
        DisableAllCurves();
    }

    private void Start()
    {
        _coinAmount = PlayerPrefs.GetInt(COIN_KEY, 0);
    }

    public CurveInfos GetCurrentCurveInfos()
    {
        CurveInfos currentCurveInfos = new CurveInfos();

        Transform currentCurveTransform = _coinCurves[_currentCurveIndex].transform;
        currentCurveInfos.curveIndex = _currentCurveIndex;
        currentCurveInfos.positionOverride = currentCurveTransform.position;
        currentCurveInfos.rotationOverride = currentCurveTransform.eulerAngles;

        return currentCurveInfos;
    }

    public void TripleCurrentCoins()
    {
        _isTripleReward = true;
        SuccessRewardPannel.Instance.UpdateRewardCoinText(TotalCoinsThisLevel * 3, true);
    }

    public void SpawnFXAtPos(Vector3 position)
    {
        _coinFX.transform.position = position;
        _coinFX.Play();
    }

    public void AddCoin()
    {
        _currentCoinThisLevel++;
      //  CurrencyManager.Instance.AddCurrency(1);
      //  PlayerPrefs.SetInt(COIN_KEY, _coinAmount);
    }

    public void AddAllCoins()
    {
        int coinsToAdd = TotalCoinsThisLevel * (_isTripleReward == true ? 3 : 1);
        _coinAmount += coinsToAdd;
        CurrencyManager.Instance.AddCurrency(coinsToAdd);
        PlayerPrefs.SetInt(COIN_KEY, _coinAmount);
    }

    public void InitializeCoinCurves(CurveInfos infos)
    {
        if (infos.curveIndex < 0)
        {
            return;
        }

        GameObject currentCurve = _coinCurves[infos.curveIndex];
        _currentCurveIndex = infos.curveIndex;
        currentCurve.transform.position = infos.positionOverride;
        currentCurve.transform.eulerAngles = infos.rotationOverride;
        currentCurve.SetActive(true);
    }

    [Button("NEXT CURVE")]
    public void NextCurve()
    {
        _currentCurveIndex++;

        if (_currentCurveIndex > _coinCurves.Length - 1)
        {
            _currentCurveIndex = 0;
        }

        DisableAllCurves();

        _coinCurves[_currentCurveIndex].SetActive(true);
    }

    private void DisableAllCurves()
    {
        for (int i = 0; i < _coinCurves.Length; i++)
        {
            _coinCurves[i].SetActive(false);
        }
    }
}

[System.Serializable]
public class CurveInfos
{
    public int curveIndex = 0;
    public Vector3 positionOverride;
    public Vector3 rotationOverride;
}
