﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameAnalyticsSDK;

public class MysteryManager : MonoBehaviourSingleton<MysteryManager>
{
    private const string DANCE_KEY = "Dance";
    private const string LAST_GIFT_INDEX_COUNT = "LastGiftIndex";
    public string DanceKey => DANCE_KEY;

    public int totalStepsToGetGift = 5;
    public Animator previewAnimator;
    public ShopItem[] _mysteryGiftItems;


    private int totalMysteryGiftCount;

    [Space(10)]
    public GameObject continueButton;
    public GameObject UnlockMysteryUnlockableButton;
    public GameObject noThanksButton;

    public Text mysteryItemPercentage;

    public float mysteryAnimationTime;
    public GameObject mysteryImage;
    public Image mysteryFillingImage;
    public GameObject previewImage;


    public ParticleSystem confettiParticles;

    public  int CurrentDanceIndex()
    {
        return PlayerPrefs.GetInt("Dance", 0);
    }


    private int currentLevel;
    private int currentMysteryGiftIndex;
    private float giftUnlockedPercentage;

 
  
    void Start()
    {
        //previewAnimator.SetInteger("Win", -1);
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.M))
        {
            DisplayMysteryPanel();
            Debug.LogError("NXNXNXN");
        }

#if UNITY_EDITOR

        if (Input.GetKeyDown(KeyCode.W))
        {
            DisplayMysteryPanel();
            UnlockMysteryGift();
        }

        if (Input.GetKeyDown(KeyCode.LeftAlt))
        {
            DisplayMysteryPanel(100);
        }

#endif
    }

   


    public void DisplayMysteryPanel()
    {
        UIController.Instance.ShowPanel(UIPanelName.MYSTERY);

        continueButton.transform.localScale = Vector3.zero;
        UnlockMysteryUnlockableButton.transform.localScale = Vector3.zero;
        noThanksButton.transform.localScale = Vector3.zero;

        currentLevel = LevelManager.Instance.CurrentLevelIndex - 1;


      //  if(currentMysteryGiftIndex >= totalMysteryGiftCount)
      //  {
      //      Debug.LogError("No more Mystery Gift to unlock -> current mystery gift index = " + currentMysteryGiftIndex);
      //      GameAnalytics.NewDesignEvent("No More Mystery Gift To Unlock.");
      //      Application.LoadLevel(Application.loadedLevel);
      //  }

        //else
        {
            if(currentLevel % totalStepsToGetGift == 0)
            {
                giftUnlockedPercentage = 100f;
            }
            else
            {
                giftUnlockedPercentage = ((currentLevel % totalStepsToGetGift) / (float)totalStepsToGetGift) * 100f;
            }

            mysteryItemPercentage.text = giftUnlockedPercentage.ToString() + "%";
            PlayMysterySuccessAnimation();
        }
    }

#if UNITY_EDITOR
    public void DisplayMysteryPanel(float openPercentage)
    {
        UIController.Instance.ShowPanel(UIPanelName.MYSTERY);

        continueButton.transform.localScale = Vector3.zero;
        UnlockMysteryUnlockableButton.transform.localScale = Vector3.zero;
        noThanksButton.transform.localScale = Vector3.zero;

        currentLevel = LevelManager.Instance.CurrentLevelIndex + 1;

        currentMysteryGiftIndex = PlayerPrefs.GetInt(LAST_GIFT_INDEX_COUNT, -1) + 1;

        if (currentMysteryGiftIndex >= totalMysteryGiftCount)
        {
            Debug.LogError("No more Mystery Gift to unlock -> current mystery gift index =  " + currentMysteryGiftIndex);
            GameAnalytics.NewDesignEvent("No More Mystery Gift To Unlock.");
            Application.LoadLevel(Application.loadedLevel);
        }

        else
        {
            giftUnlockedPercentage = openPercentage;
            mysteryItemPercentage.text = giftUnlockedPercentage.ToString() + "%";
            PlayMysterySuccessAnimation();
        }
    }

    void PlayMysterySuccessAnimation()
    {
        mysteryItemPercentage.text = (giftUnlockedPercentage - 100 / totalStepsToGetGift).ToString("0") + "%";
        LeanTween.value(mysteryItemPercentage.gameObject, UpdatePercentageText, giftUnlockedPercentage - 100 / totalStepsToGetGift, giftUnlockedPercentage, mysteryAnimationTime).setEaseInOutSine().setOnComplete(MysteryAnimationComplete);
        LaunchLoopingVibrations(mysteryAnimationTime, 0.2f);
    }
#endif

    void UpdatePercentageText(float value)
    {
        mysteryItemPercentage.text = value.ToString("0") + "%";
        mysteryFillingImage.fillAmount = value / 100f;
    }

    void MysteryAnimationComplete()
    {
        if (giftUnlockedPercentage >= 100f)
        {
            DisplayUnlockOptions();
            mysteryItemPercentage.gameObject.SetActive(false);
        }
        else
        {
            DisplayContinueButton();
        }
    }

    void DisplayContinueButton()
    {
        LeanTween.cancel(continueButton);
        LeanTween.scale(continueButton, Vector3.one, 0.3f).setEaseOutBack();

        LeanTween.cancel(noThanksButton);
        LeanTween.scale(noThanksButton, Vector3.zero, 0.2f).setEaseOutBack();
    }

    void DisplayUnlockOptions()
    {
        LeanTween.cancel(UnlockMysteryUnlockableButton);
        LeanTween.scale(UnlockMysteryUnlockableButton, Vector3.one, 0.3f).setEaseOutBack();

        LeanTween.cancel(noThanksButton);
        LeanTween.scale(noThanksButton, Vector3.one, 0.3f).setEaseOutBack();
    }

    public void UnlockMysteryGift()
    {
        Debug.LogError("UNLOCKING MYSTERY GIFT");
       // ShopItem item = ShopManager.Instance.GetRandomItemNotUnlocked(ShopCategoryItem.Handle);
        PlayMysteryGiftAnimation();
    }

    #region Unlock Sequences

    void PlayMysteryGiftAnimation()
    {
        UnlockSequencePartOne();
    }

    void UnlockSequencePartOne()
    {
        LeanTween.moveLocalX(mysteryImage, mysteryImage.transform.localPosition.x - 10f, 0.02f).setLoopPingPong(5);
        LeanTween.moveLocalY(mysteryImage, mysteryImage.transform.localPosition.y + 10f, 0.02f).setLoopPingPong(5).setOnComplete(UnlockSequencePartTwo);
        LaunchLoopingVibrations(0.2f, 0.04f);
    }

    void UnlockSequencePartTwo()
    {
        LeanTween.scaleX(mysteryImage, 0.5f, 0.5f).setEaseOutSine();
        LeanTween.scaleY(mysteryImage, 1.25f, 0.5f).setEaseOutSine().setOnComplete(UnlockSequencePartThree);
    }

    public void HideMysteryPannel()
    {
        UIController.Instance.HidePanelInstantly(UIPanelName.MYSTERY);
        GameManager.Instance.OnMysteryGiftEnded();
    }

    void UnlockSequencePartThree()
    {
        LeanTween.scale(mysteryImage, Vector3.zero, 0.1f);
        previewImage.SetActive(true);

        confettiParticles.Play();

        ShopItem item = ShopManager.Instance.GetRandomItemNotUnlocked(ShopCategoryItem.Handle);
        ShopItem_Spawnable itemEqui = (ShopItem_Spawnable)item;

        GameObject g = Instantiate(itemEqui.ObjectToSpawn, transform.position + transform.forward * 4 - Vector3.up, Quaternion.identity, transform);
       RotationScript s =  g.AddComponent<RotationScript>();
        s.rotationSpeed = Vector3.up * 50;
        item.Unlock();

        VibrationManager.VibrateSuccess();

        DisplayContinueButton();
    }

    #endregion

    #region  Vibrations
         void LaunchLoopingVibrations(float duration, float frequency)
         {
             InvokeRepeating("VibrateLight", 0f, frequency);
             Invoke("StopLoopingVibrations", duration);
         }

         void StopLoopingVibrations()
         {
             CancelInvoke("VibrateLight");
         }

         void VibrateLight()
         {
             VibrationManager.VibrateLight();
         }
    #endregion

}
