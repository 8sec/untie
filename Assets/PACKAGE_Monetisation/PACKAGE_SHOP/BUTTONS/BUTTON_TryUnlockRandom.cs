﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using GameAnalyticsSDK;
[RequireComponent(typeof(Button))]
public class BUTTON_TryUnlockRandom : MonoBehaviour
{
    Button m_Button;
    public Text m_PriceDisplayer;

    // Start is called before the first frame update
    void Start()
    {
        m_Button = GetComponent<Button>();
        InvokeRepeating("UpdateButtonState", 0.1f, 0.5f);
    }
   
    void UpdateButtonState()
    {

        int unlockPrice = ShopManager.Instance.CurrentUnlockPrice;
        m_PriceDisplayer.text = unlockPrice.ToString();

        if (CurrencyManager.Instance == null)
        {
            Debug.LogError("Your Currency Manager Instance is null. " + gameObject.name + " can't update his state");
            return;
        }

        m_Button.interactable = CurrencyManager.Instance.totalCurrency >= unlockPrice && ShopManager.Instance.CanInteract;

        
    }
    public void Action()
    {
        ShopManager.Instance.TryUnlockRandom();
    }
}
