﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RB_SkipLevel : RewardedButton
{
    public override void CollectReward()
    {
        base.CollectReward();
        GameManager.Instance.LevelSuccess();
    }
}
