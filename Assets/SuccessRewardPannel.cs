﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SuccessRewardPannel : MonoBehaviour
{
    [SerializeField] private Text _coinText;
    [SerializeField] private GameObject _keyReward;

    public static SuccessRewardPannel Instance;

    private void Awake()
    {
        Instance = this;
    }

    private void OnEnable()
    {
        UpdateRewardCoinText(CoinManager.Instance.TotalCoinsThisLevel);
    }

    public void UpdateRewardCoinText(int amount, bool scaleUp = false)
    {
        _coinText.text = amount.ToString();

        if (TR_KeyManager.Instance.HasPickedUpKeyThisLevel == false)
        {
            _keyReward.SetActive(false);
        }

        else
        {
            _keyReward.SetActive(true);
        }

        if (scaleUp == true)
        {
            LeanTween.scale(_coinText.gameObject, _coinText.transform.localScale + Vector3.one * 0.3f, 0.2f).setLoopPingPong(1);
        }
    }

}
