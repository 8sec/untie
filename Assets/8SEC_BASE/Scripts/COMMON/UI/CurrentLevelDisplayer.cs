﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Text))]
public class CurrentLevelDisplayer : MonoBehaviour {
    private Text t;
    public string Prefix = "Level ";
    public string Suffix;
    [Tooltip ("Set to 0 to have current Level, -1 to have previous, +1 to have next")]
    public int IndexOffset = 0;
    private void Awake () {
        t = GetComponent<Text> ();
    }
    // Start is called before the first frame update
    void Start () {
        UpdateDisplay ();

    }

    // Update is called once per frame
    void Update () {

    }

    private void OnEnable () {
        UpdateDisplay ();
    }

    void UpdateDisplay () {
        if (LevelManager.Instance != null)
            t.text = Prefix + (LevelManager.Instance.CurrentLevelIndex + IndexOffset).ToString ("0") + Suffix;
        else
            t.text = "";
    }
}