﻿using System;
using System.Collections.Generic;
using GameAnalyticsSDK;
using UnityEngine;
using UnityEngine.Purchasing;

// Deriving the Purchaser class from IStoreListener enables it to receive messages from Unity Purchasing.
public class IAPManager : MonoBehaviour, IStoreListener {
    public static IAPManager Instance;
    private static IStoreController m_StoreController; // The Unity Purchasing system.
    private static IExtensionProvider m_StoreExtensionProvider; // The store-specific Purchasing subsystems.

    // Product identifiers for all products capable of being purchased: 
    // "convenience" general identifiers for use with Purchasing, and their store-specific identifier 
    // counterparts for use with and outside of Unity Purchasing. Define store-specific identifiers 
    // also on each platform's publisher dashboard (iTunes Connect, Google Play Developer Console, etc.)

    // General product identifiers for the consumable, non-consumable, and subscription products.
    // Use these handles in the code to reference which product to purchase. Also use these values 
    // when defining the Product Identifiers on the store. Except, for illustration purposes, the 
    // kProductIDSubscription - it has custom Apple and Google identifiers. We declare their store-
    // specific mapping to Unity Purchasing's AddProduct, below.
    public static string kProductIDConsumable = "xx";
    public static string kProductID_NoAds_Normal = "jr_noads";
#if UNITY_IOS
    public static string kProductID_NoAds_Sale = "noads_SALE";
#else
    public static string kProductID_NoAds_Sale = "noads_sale";

#endif
    public static string kProductIDSubscription = "subscription";

    // Apple App Store-specific product identifier for the subscription product.
    private static string kProductNameAppleSubscription = "com.unity3d.subscription.new";

    // Google Play Store-specific product identifier subscription product.
    private static string kProductNameGooglePlaySubscription = "com.unity3d.subscription.original";
    private void Awake () {
        if (Instance == null) {
            Instance = this;
            DontDestroyOnLoad (this);
            DontDestroyOnLoad (gameObject);
        } else {

            Destroy (gameObject);
        }

    }
    void Start () {
        // If we haven't set up the Unity Purchasing reference
        if (m_StoreController == null) {
            // Begin to configure our connection to Purchasing
            InitializePurchasing ();
        }
    }

    public void InitializePurchasing () {
        // If we have already connected to Purchasing ...
        if (IsInitialized ()) {
            // ... we are done here.
            return;
        }

        // Create a builder, first passing in a suite of Unity provided stores.
        var builder = ConfigurationBuilder.Instance (StandardPurchasingModule.Instance ());

        // Add a product to sell / restore by way of its identifier, associating the general identifier
        // with its store-specific identifiers.
        /* IF WE HAD A CONSUMABLE
        builder.AddProduct (kProductIDConsumable, ProductType.Consumable);
        */
        // Continue adding the non-consumable product.
        builder.AddProduct (kProductID_NoAds_Normal, ProductType.NonConsumable);
        builder.AddProduct (kProductID_NoAds_Sale, ProductType.NonConsumable);
        // And finish adding the subscription product. Notice this uses store-specific IDs, illustrating
        // if the Product ID was configured differently between Apple and Google stores. Also note that
        // one uses the general kProductIDSubscription handle inside the game - the store-specific IDs 
        // must only be referenced here. 
        /*  IF WE HAD A SUBSCRIPTION
        builder.AddProduct (kProductIDSubscription, ProductType.Subscription, new IDs () { { kProductNameAppleSubscription, AppleAppStore.Name }, { kProductNameGooglePlaySubscription, GooglePlay.Name },
        });
        */
        // Kick off the remainder of the set-up with an asynchrounous call, passing the configuration 
        // and this class' instance. Expect a response either in OnInitialized or OnInitializeFailed.
        UnityPurchasing.Initialize (this, builder);
    }

    private bool IsInitialized () {
        // Only say we are initialized if both the Purchasing references are set.   
        return m_StoreController != null && m_StoreExtensionProvider != null;
    }

    public void BuyConsumable () {
        // Buy the consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID (kProductIDConsumable);
        System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
        richEvent.Add("af_productId", kProductIDConsumable);
        AppsFlyer.trackRichEvent("af_IAP_Consumable_Clicked", richEvent);
    }

    public void Buy_NoAds_Normal () {
        if (UIController.Instance != null)
            UIController.Instance.SetLoadingLockScreenState(true);
        // Buy the non-consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID (kProductID_NoAds_Normal);
        
        GameAnalytics.NewDesignEvent ("IAP:NoAds:Clicked");
        System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
        richEvent.Add("af_productId",kProductID_NoAds_Normal);
        AppsFlyer.trackRichEvent("af_IAP_NoAds_Clicked", richEvent);
    }

    public void Buy_NoAds_Sale()
    {
        if (UIController.Instance != null)
            UIController.Instance.SetLoadingLockScreenState(true);
        // Buy the non-consumable product using its general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        BuyProductID(kProductID_NoAds_Sale);

        GameAnalytics.NewDesignEvent("IAP:NoAdsSALE:Clicked");
        System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
        richEvent.Add("af_productId", kProductID_NoAds_Sale);
        AppsFlyer.trackRichEvent("af_IAP_NoAdsSALE_Clicked", richEvent);
    }

    public void BuySubscription () {
        if (UIController.Instance != null)
            UIController.Instance.SetLoadingLockScreenState(true);
        // Buy the subscription product using its the general identifier. Expect a response either 
        // through ProcessPurchase or OnPurchaseFailed asynchronously.
        // Notice how we use the general product identifier in spite of this ID being mapped to
        // custom store-specific identifiers above.
        BuyProductID (kProductIDSubscription);
        System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();

        richEvent.Add("af_productId", kProductIDSubscription);
        AppsFlyer.trackRichEvent("af_IAP_Subscription_Clicked", richEvent);
    }

    void BuyProductID (string productId) {
        if (UIController.Instance != null)
            UIController.Instance.SetLoadingLockScreenState(true);
        // If Purchasing has been initialized ...
        if (IsInitialized ()) {
            // ... look up the Product reference with the general product identifier and the Purchasing 
            // system's products collection.
            Product product = m_StoreController.products.WithID (productId);

            // If the look up found a product for this device's store and that product is ready to be sold ... 
            if (product != null && product.availableToPurchase) {
                Debug.Log (string.Format ("Purchasing product asychronously: '{0}'", product.definition.id));
                // ... buy the product. Expect a response either through ProcessPurchase or OnPurchaseFailed 
                // asynchronously.
                m_StoreController.InitiatePurchase (product);
            }
            // Otherwise ...
            else {
                // ... report the product look-up failure situation  
                Debug.Log ("BuyProductID: FAIL. Not purchasing product, either is not found or is not available for purchase");
            }
        }
        // Otherwise ...
        else {
            // ... report the fact Purchasing has not succeeded initializing yet. Consider waiting longer or 
            // retrying initiailization.
            Debug.Log ("BuyProductID FAIL. Not initialized.");
            if (UIController.Instance != null)
                UIController.Instance.SetLoadingLockScreenState(false);
        }
    }

    // Restore purchases previously made by this customer. Some platforms automatically restore purchases, like Google. 
    // Apple currently requires explicit purchase restoration for IAP, conditionally displaying a password prompt.
    public void RestorePurchases () {

         // If Purchasing has not yet been set up ...
        if (!IsInitialized ()) {
            // ... report the situation and stop restoring. Consider either waiting longer, or retrying initialization.
            Debug.Log ("RestorePurchases FAIL. Not initialized.");
            if (UIController.Instance != null)
                UIController.Instance.SetLoadingLockScreenState(false);
            return;
        }
       
        // If we are running on an Apple device ... 
        if (Application.platform == RuntimePlatform.IPhonePlayer ||
            Application.platform == RuntimePlatform.OSXPlayer) {
            if (UIController.Instance != null)
                UIController.Instance.SetLoadingLockScreenState(true);
            // ... begin restoring purchases
            Debug.Log ("RestorePurchases started ...");
            GameAnalytics.NewDesignEvent ("IAP:RestoreIOS");
            System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
            
            AppsFlyer.trackRichEvent("af_IAP_RestorePurchases", richEvent);
            // Fetch the Apple store-specific subsystem.
            var apple = m_StoreExtensionProvider.GetExtension<IAppleExtensions>();
            // Begin the asynchronous process of restoring purchases. Expect a confirmation response in 
            // the Action<bool> below, and ProcessPurchase if there are previously purchased products to restore.
            apple.RestoreTransactions ((result) => {
                // The first phase of restoration. If no more responses are received on ProcessPurchase then 
                // no purchases are available to be restored.
                Debug.Log ("RestorePurchases continuing: " + result + ". If no further messages, no purchases available to restore.");
                if (UIController.Instance != null)
                    UIController.Instance.SetLoadingLockScreenState(false);
            });
        }
        // Otherwise ...
        else {
            // We are not running on an Apple device. No work is necessary to restore purchases.
            Debug.Log ("RestorePurchases FAIL. Not supported on this platform. Current = " + Application.platform);
            if (UIController.Instance != null)
                UIController.Instance.SetLoadingLockScreenState(false);
        }
    }

    //  
    // --- IStoreListener
    //
    public static string NoAds_NormalPrice = "BUY";
    public static string NoAds_SalePrice = "BUY";
    public void OnInitialized (IStoreController controller, IExtensionProvider extensions) {
        // Purchasing has succeeded initializing. Collect our Purchasing references.
        Debug.Log ("OnInitialized: PASS");
        // Overall Purchasing system, configured with products for this application.
        m_StoreController = controller;
        // Store specific subsystem, for accessing device-specific store features.
        m_StoreExtensionProvider = extensions;

        var pc = m_StoreController.products;
        var noAdsNormal = pc.WithID(kProductID_NoAds_Normal);
        var noAdsSale = pc.WithID(kProductID_NoAds_Sale);
        NoAds_NormalPrice = noAdsNormal.metadata.localizedPriceString;
        NoAds_SalePrice = noAdsSale.metadata.localizedPriceString;
    }

    public void OnInitializeFailed (InitializationFailureReason error) {
        // Purchasing set-up has not succeeded. Check error for reason. Consider sharing this reason with the user.
        Debug.Log ("OnInitializeFailed InitializationFailureReason:" + error);
    }

    public PurchaseProcessingResult ProcessPurchase (PurchaseEventArgs args) {
        if (UIController.Instance != null)
            UIController.Instance.SetLoadingLockScreenState(false);
        // A consumable product has been purchased by this user.
        if (String.Equals (args.purchasedProduct.definition.id, kProductIDConsumable, StringComparison.Ordinal)) {
            Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // The consumable item has been successfully purchased, add 100 coins to the player's in-game score.
            System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
            richEvent.Add("af_productId", kProductIDConsumable);
            AppsFlyer.trackRichEvent("af_IAP_Consumable_Purchased", richEvent);
        }
        // Or ... a non-consumable product has been purchased by this user.
        else if (String.Equals (args.purchasedProduct.definition.id, kProductID_NoAds_Normal, StringComparison.Ordinal)) {
            Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
            Debug.Log ("Enabling NO ADS Mode");

            UserConfig.NoAds = true;
            MonetizationManager.Instance.RemoveAds ();
            GameAnalytics.NewDesignEvent ("IAP:NoAds:Purchased");
            System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
            richEvent.Add("af_productId", kProductID_NoAds_Normal);
            AppsFlyer.trackRichEvent("af_IAP_NoAds_Purchased", richEvent);

        }
        else if (String.Equals(args.purchasedProduct.definition.id, kProductID_NoAds_Sale, StringComparison.Ordinal))
        {
            Debug.Log(string.Format("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // TODO: The non-consumable item has been successfully purchased, grant this item to the player.
            Debug.Log("Enabling NO ADS Mode");

            UserConfig.NoAds = true;
            MonetizationManager.Instance.RemoveAds();
            GameAnalytics.NewDesignEvent("IAP:NoAdsSALE:Purchased");
            System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
            richEvent.Add("af_productId", kProductID_NoAds_Sale);
            AppsFlyer.trackRichEvent("af_IAP_NoAdsSALE_Purchased", richEvent);

        }
        // Or ... a subscription product has been purchased by this user.
        else if (String.Equals (args.purchasedProduct.definition.id, kProductIDSubscription, StringComparison.Ordinal)) {
            Debug.Log (string.Format ("ProcessPurchase: PASS. Product: '{0}'", args.purchasedProduct.definition.id));
            // TODO: The subscription item has been successfully purchased, grant this to the player.
            System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
            richEvent.Add("af_productId", kProductIDConsumable);
            AppsFlyer.trackRichEvent("af_IAP_Subscription_Purchased", richEvent);
        }
        // Or ... an unknown product has been purchased by this user. Fill in additional products here....
        else {
            Debug.Log (string.Format ("ProcessPurchase: FAIL. Unrecognized product: '{0}'", args.purchasedProduct.definition.id));
            System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
            richEvent.Add("af_productId", kProductIDConsumable);

            AppsFlyer.trackRichEvent("af_IAP_Failed", richEvent);
            if (UIController.Instance != null)
                UIController.Instance.SetLoadingLockScreenState(false);
        }

        // Return a flag indicating whether this product has completely been received, or if the application needs 
        // to be reminded of this purchase at next app launch. Use PurchaseProcessingResult.Pending when still 
        // saving purchased products to the cloud, and when that save is delayed. 
        return PurchaseProcessingResult.Complete;
    }

    public void OnPurchaseFailed (Product product, PurchaseFailureReason failureReason) {
        if (UIController.Instance != null)
            UIController.Instance.SetLoadingLockScreenState(false);
        // A product purchase attempt did not succeed. Check failureReason for more detail. Consider sharing 
        // this reason with the user to guide their troubleshooting actions.
        Debug.Log (string.Format ("OnPurchaseFailed: FAIL. Product: '{0}', PurchaseFailureReason: {1}", product.definition.storeSpecificId, failureReason));
        System.Collections.Generic.Dictionary<string, string> richEvent = new System.Collections.Generic.Dictionary<string, string>();
        richEvent.Add("af_productId", product.definition.storeSpecificId);
        richEvent.Add("af_failureReason", failureReason.ToString());
        
        AppsFlyer.trackRichEvent("af_IAP_Failed", richEvent);
    }

    
}