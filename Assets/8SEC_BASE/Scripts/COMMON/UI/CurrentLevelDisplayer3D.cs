﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class CurrentLevelDisplayer3D : MonoBehaviour {
    private TextMeshPro t;
    public string Prefix = "Level ";
    public string Suffix;
    [Tooltip ("Set to 0 to have current Level, -1 to have previous, +1 to have next")]
    public int IndexOffset = 0;
    private void Awake () {
        t = GetComponent<TextMeshPro> ();
    }
    // Start is called before the first frame update
    void Start () {
        UpdateDisplay ();

    }

    // Update is called once per frame
    void Update () {

    }

    private void OnEnable () {
        UpdateDisplay ();
    }

    void UpdateDisplay () {
        if (LevelManager.Instance != null)
            t.text = Prefix + (LevelManager.Instance.CurrentLevelIndex + IndexOffset).ToString ("0") + Suffix;
        else
            t.text = "";
    }
}