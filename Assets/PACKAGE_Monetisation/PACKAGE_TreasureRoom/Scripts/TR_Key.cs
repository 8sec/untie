﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using PathCreation;
using DG.Tweening;

public class TR_Key : MonoBehaviour
{
    bool _hasBeenPickedUp;
   public void RecoltKey()
   {
        GetComponent<PathCreation.Examples.PathFollower>().enabled = false;
        GetComponent<Collider>().enabled = false;
        KeyRecoltedAnimation();
        TR_KeyManager.Instance.AddKey(1);
        TR_KeyManager.Instance.OnKeyPickedUp();
        VibrationManager.VibrateHeavy();
   }

    private void KeyRecoltedAnimation()
    {
        if (_hasBeenPickedUp)
        {
            return;
        }

        _hasBeenPickedUp = true;

       
        //LeanTween.rotateAround(gameObject, Vector3.up, 360, 0.2f).setOnComplete(c => { LeanTween.scale(gameObject, Vector3.zero, 0.2f).setEaseOutBack().setOnComplete(d => gameObject.SetActive(false)); });

        Sequence s = DOTween.Sequence();
        s.Append(transform.DORotate(transform.eulerAngles + Vector3.up * 360, 0.3f, RotateMode.FastBeyond360));
        s.Append(transform.DOScale(Vector3.zero, 0.2f).SetEase(Ease.InBack));

    }
}
