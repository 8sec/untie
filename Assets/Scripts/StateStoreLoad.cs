﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;
using Sirenix.OdinInspector;
using UnityEditor;
using System.IO;
using System.Linq;

public class StateStoreLoad : MonoBehaviour
{
    public static StateStoreLoad Instance;
 
    public string RopeLoadStateA;
    public string RopeLoadStateB;

    public List<ObiRope> RopeActors = new List<ObiRope>();

    private const string FILE_PATH = "LevelTXT.txt";
    [SerializeField] private List<LevelTemplate> _allLevels = new List<LevelTemplate>();
    [SerializeField] private List<LevelTemplate> _easyLevels = new List<LevelTemplate>();
    [SerializeField] private List<LevelTemplate> _normalLevels = new List<LevelTemplate>();
    [SerializeField] private List<LevelTemplate> _levelsAfter26 = new List<LevelTemplate>();

    public LevelTemplate _debugLevel;
    [SerializeField] private LevelTemplate[] _premiumLevels;

    private string _tempLevel;
    private LevelTemplate _currentLevelTemplate;


    private void Awake()
    {
        Instance = this;

    }
    private void Start()
    {
        ResetVelocities();
        Invoke("LoadCurrentLevel", 0.04f);
    
    }

    void LoadCurrentLevel()
    {
        LoadLevel(LevelManager.Instance.CurrentLevelIndex);
        if (LevelManager.Instance.CurrentLevelIndex == 11)
        {
            UIController.Instance.EnableHelpPannelHandles();
        }

        else if (LevelManager.Instance.CurrentLevelIndex == 22)
        {
            UIController.Instance.EnableHelpPannelObstacles();
        }
        ResetVelocities();

    }

    public void PlaySpecialLevel()
    {
        LevelTemplate randomPremiumLevel = _premiumLevels[Random.Range(0, _premiumLevels.Length)];
        LoadFromStringMultiple(randomPremiumLevel.ropes, true);
        ResetVelocities();
    }

    public void Update()
    {
#if UNITY_EDITOR
        if (Input.GetKeyDown(KeyCode.S))
        {
            
            AddLevelToList();
        }
         if(Input.GetKeyDown(KeyCode.L))
        {
            LoadLevel(LevelManager.Instance.CurrentLevelIndex);
        }
        if (Input.GetKeyDown(KeyCode.R))
        {
            LoadLevel(0);
        }

        if (Input.GetKeyDown(KeyCode.F))
            SetFlipSolver(!solverFlipped);
#endif
    }

    public void LoadFromStringMultiple(string level, bool debug = false)
    {

        Debug.LogError(_currentLevelTemplate);
        if (debug == false)
        {
            ObstacleManager.Instance.InitializeObstacles(_currentLevelTemplate.obstacleInfos);
            GroundManager.Instance.InitializeGround(_currentLevelTemplate.groundIndex);
            CoinManager.Instance.InitializeCoinCurves(_currentLevelTemplate.curveInfos);
        }

        else
        {
            ObstacleManager.Instance.InitializeObstacles(_debugLevel.obstacleInfos);
            GroundManager.Instance.InitializeGround(_debugLevel.groundIndex);
            CoinManager.Instance.InitializeCoinCurves(_debugLevel.curveInfos);
        }

        foreach (var r in RopeActors)
        {
            r.GetComponent<Rope>().SetRopeState(false);
        }

        var split = level.Split('|');
        int ropeCount = split.Length;
        Debug.Log("split length : " + ropeCount);
        
        for (int i = 0; i < ropeCount; i++)
        {
            var rope = RopeActors[i].GetComponent<Rope>();
            RopeActors[i].gameObject.SetActive(true);
            rope.SetRopeState(true);
            rope.DetachAnchors();
            var a = ScriptableObject.CreateInstance<ObiRopeBlueprint>();
            var json = split[i];
            JsonUtility.FromJsonOverwrite(json, a);
            RopeActors[i].ropeBlueprint = a;
            RopeActors[i].GetComponent<Rope>().SetAnchorsOnRopeEnds();

            switch (i)
            {
                case 0:

                    if (_currentLevelTemplate._hasAnchorRopeOneA)
                    {
                        rope.SetUnmovable(false, AnchorPoint.A);
                    }

                    if (_currentLevelTemplate._hasAnchorRopeOneB)
                    {
                        rope.SetUnmovable(false, AnchorPoint.B);
                    }
                    break;
                case 1:
                    if (_currentLevelTemplate._hasAnchorRopeTwoA)
                    {
                        rope.SetUnmovable(false, AnchorPoint.A);
                    }

                    if (_currentLevelTemplate._hasAnchorRopeTwoB)
                    {
                        rope.SetUnmovable(false, AnchorPoint.B);
                    }
                    break;
                case 2:
                    if (_currentLevelTemplate._hasAnchorRopeThreeA)
                    {
                        rope.SetUnmovable(false, AnchorPoint.A);
                    }

                    if (_currentLevelTemplate._hasAnchorRopeThreeB)
                    {
                        rope.SetUnmovable(false, AnchorPoint.B);
                    }
                    break;
                case 3:
                    if (_currentLevelTemplate._hasAnchorRopeFourA)
                    {
                        rope.SetUnmovable(false, AnchorPoint.A);
                    }

                    if (_currentLevelTemplate._hasAnchorRopeFourB)
                    {
                        rope.SetUnmovable(false, AnchorPoint.B);
                    }
                    break;
                case 4:
                    if (_currentLevelTemplate._hasAnchorRopeFiveA)
                    {
                        rope.SetUnmovable(false, AnchorPoint.A);
                    }

                    if (_currentLevelTemplate._hasAnchorRopeFiveB)
                    {
                        rope.SetUnmovable(false, AnchorPoint.B);
                    }
                    break;
                default:
                    break;
            }


        }



    }

    public bool ShowStrings = false;
    public string DefaultLevelString;
   // [ShowIf("ShowStrings")]
   // public List<string> LevelStrings = new List<string>();
  //  [ShowIf("ShowStrings")]
  //  public List<string> EasyLevelStrings = new List<string>();
  //  [ShowIf("ShowStrings")]
  //  public List<string> LevelStrings_Original = new List<string>();

    [Button("Load random bowl")]
    public void LoadRandomLevel()
    {
        LoadLevel(Random.Range(0, _allLevels.Count));
    }
    [Header("DEBUG Levels")]
    public int DEBUG_LevelIndex = 0;
    [Button("Load debug level")] 
    public void DEBUG_LoadLevel()
    {
        Debug.Log("LOADING LEVEL : " + DEBUG_LevelIndex);
        LoadDebugLevel();
    }

   // [Button("CREATE TXT FILE OF LEVELS")]
   // public void WriteAllLevelsOnTXT()
   // {
   //     if (File.Exists(FILE_PATH))
   //     {
   //         File.Delete(FILE_PATH);
   //     }
   //
   //     FileStream file = File.Open(FILE_PATH, FileMode.OpenOrCreate);
   //     var writter = new StreamWriter(file);
   //
   //     for (int i = 0; i < LevelStrings.Count; i++)
   //     {
   //         writter.Write(LevelStrings[i]+"\n");
   //     }
   // }

    public void LoadDebugLevel()
    {
        string level = _debugLevel.ropes;
        LoadFromStringMultiple(level, true);
        ResetVelocities();
    }

    public void LoadLevel(int levelIndex)
    {

        int clampIndex = levelIndex;// levelIndex % LevelStrings.Count;

        if (clampIndex > _allLevels.Count-1)
        {
            // On fait en sorte que tout le monde ait le même random dans l'ordre des niveaux :
            var state = Random.state;
            Random.InitState(levelIndex);
            clampIndex = Random.Range(0, _allLevels.Count);
            Random.state = state;//(puis on rétablit le random)
        }
        Debug.Log("Loading level index : " + clampIndex);
        // string level = LevelStrings[clampIndex];
        _currentLevelTemplate = _allLevels[clampIndex];
        string level = _currentLevelTemplate.ropes;


        LoadFromStringMultiple(level);

        ResetVelocities();
        return;

      
    }

    public ObiSolver Solver;

    [Button("ADD LEVELS AFTER 26")]
    public void AddLevelAfter26()
    {
        _allLevels.InsertRange(27,_levelsAfter26);
    }

    void ResetVelocities()
    {
        for (int i = 0; i < Solver.velocities.count; i++)
        {
            Solver.velocities[i] = Vector4.zero;
        }
        for (int i = 0; i < Solver.angularVelocities.count; i++)
        {
            Solver.angularVelocities[i] = Vector4.zero;
        }
    }

#if UNITY_EDITOR
    [Button("Save level string")]
    public void AddLevelToList()
    {
        string lvl ="";
        int ropes = 0;
        LevelTemplate newLevel = CreateLevelTemplate<LevelTemplate>("TempLevel", "Assets/LevelTemplate");

        for (int i = 0; i < RopeActors.Count; i++)
        {

            if (RopeActors[i].gameObject.activeSelf)
            {
                ropes++;
                var bp = Instantiate(RopeActors[i].blueprint);
                RopeActors[i].SaveStateToBlueprint(bp);
                lvl += JsonUtility.ToJson(bp) + '|' ;
                Rope rope = RopeActors[i].GetComponent<Rope>();



                switch (i)
                {
                    case 0:
                        if (rope.DraggableAnchorA.hasWeight)
                        {
                            newLevel._hasAnchorRopeOneA = true;
                            newLevel.hasAnchor = true;
                        }

                        if (rope.DraggableAnchorB.hasWeight)
                        {
                            newLevel._hasAnchorRopeOneB = true;
                            newLevel.hasAnchor = true;

                        }
                        break;
                    case 1:
                        if (rope.DraggableAnchorA.hasWeight)
                        {
                            newLevel._hasAnchorRopeTwoA = true;
                            newLevel.hasAnchor = true;

                        }

                        if (rope.DraggableAnchorB.hasWeight)
                        {
                            newLevel._hasAnchorRopeTwoB = true;
                            newLevel.hasAnchor = true;

                        }
                        break;
                    case 2:
                        if (rope.DraggableAnchorA.hasWeight)
                        {
                            newLevel._hasAnchorRopeThreeA = true;
                            newLevel.hasAnchor = true;

                        }

                        if (rope.DraggableAnchorB.hasWeight)
                        {
                            newLevel._hasAnchorRopeThreeB = true;
                            newLevel.hasAnchor = true;

                        }
                        break;
                    case 3:
                        if (rope.DraggableAnchorA.hasWeight)
                        {
                            newLevel._hasAnchorRopeFourA = true;
                            newLevel.hasAnchor = true;

                        }

                        if (rope.DraggableAnchorB.hasWeight)
                        {
                            newLevel._hasAnchorRopeFourB= true;
                            newLevel.hasAnchor = true;

                        }
                        break;
                    case 4:
                        if (rope.DraggableAnchorA.hasWeight)
                        {
                            newLevel._hasAnchorRopeFiveA = true;
                            newLevel.hasAnchor = true;

                        }

                        if (rope.DraggableAnchorB.hasWeight)
                        {
                            newLevel._hasAnchorRopeFiveB = true;
                            newLevel.hasAnchor = true;

                        }
                        break;
                    default:
                        break;
                }
            }
        }

        lvl = lvl.TrimEnd('|');

         newLevel.ropes = lvl;

        #region Obstacle Handling

        ObstacleInfo currentObstacleInfos = ObstacleManager.Instance.GetCurrentObstacleInfos();

        newLevel.obstacleInfos.obstacleIndex = currentObstacleInfos.obstacleIndex;


        if (newLevel.HasObstacle)
        {
            newLevel.obstacleInfos.overridePosition = currentObstacleInfos.overridePosition;
            newLevel.obstacleInfos.overrideRotation = currentObstacleInfos.overrideRotation;
        }

        #endregion
        #region Coin Curve Handling

        CurveInfos currentCurveInfos = CoinManager.Instance.GetCurrentCurveInfos();

        newLevel.curveInfos.curveIndex = currentCurveInfos.curveIndex;


        if (newLevel.HasCoinCurve)
        {
            newLevel.curveInfos.positionOverride = currentCurveInfos.positionOverride;
            newLevel.curveInfos.rotationOverride = currentCurveInfos.rotationOverride;
        }


        #endregion

        newLevel.groundIndex = GroundManager.Instance.CurrentGroundIndex;


        EditorUtility.SetDirty(newLevel);
        AssetDatabase.SaveAssets();
    }

#endif

    public Transform SolverTransform;
    bool solverFlipped = false;
    public void SetFlipSolver(bool flipped)
    {
        if( SolverTransform == null)
        {
            Debug.LogError("Solver Transform must be filled!");
            return;
        }
        solverFlipped = flipped;

        if (flipped)
        {
            SolverTransform.position = new Vector3(0f, 0f, -2f);
            SolverTransform.localEulerAngles = new Vector3(0f, 180F, 0f);
        }
        else
        {
            SolverTransform.position = new Vector3(0f, 0f,0f);
            SolverTransform.localEulerAngles = new Vector3(0f,0F, 0f);
        }
    }
    [Button("Shuffle Normal levels")]
    public void ShuffleEasyLevels()
    {
        _normalLevels.Shuffle();
    }

    public List<Material> RopeColors = new List<Material>();
    public List<Material> RopeEndColors = new List<Material>();
    
    [Button("Generate level list")]
    public void MergeLevelLists() {
        _allLevels.Clear();
        int easy = 0;
        int normal = 0;
 
 
        for (int i = 0; i < 6; i++)
        {
            _allLevels.Add(_easyLevels[easy]);
            easy++;
        }
       
 
        while (easy < _easyLevels.Count-2 && normal < _allLevels.Count)
        {
            _allLevels.Add(_normalLevels[normal]);
            normal++;
            _allLevels.Add(_easyLevels[easy]);
            easy++;
            _allLevels.Add(_easyLevels[easy]);
            easy++;
        }
 
        while (normal < _allLevels.Count)
        {
            _allLevels.Add(_normalLevels[normal]);
            normal++;
        }
    
    
    }

#if UNITY_EDITOR

    public static T[] GetAllInstances<T>(string[] path) where T : ScriptableObject
    {
        string[] guids = AssetDatabase.FindAssets("t:" + typeof(T).Name, path); 
        T[] a = new T[guids.Length];
        for (int i = 0; i < guids.Length; i++)       
        {
            string newPath = AssetDatabase.GUIDToAssetPath(guids[i]);
            a[i] = AssetDatabase.LoadAssetAtPath<T>(newPath);
        }

        return a;

    }


    public static LevelTemplate CreateLevelTemplate<T>(string assetName, string path = "") where T : LevelTemplate
    {
        T asset = ScriptableObject.CreateInstance<T>();

        string assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(path + "/" + assetName + ".asset");
        AssetDatabase.CreateAsset(asset, assetPathAndName);

        AssetDatabase.SaveAssets();
        AssetDatabase.Refresh();
        EditorUtility.FocusProjectWindow();
        Selection.activeObject = asset;
        return asset;
    }

    [Button("FILL LEVELTEMPLATES IN PROJECT")]
    public void FillLevelTemplate()
    {
        

        string path = FILE_PATH;
        string[] lines = File.ReadAllLines(path);

       

         for (int i = 6; i < lines.Length; i++)
         {
             if (i < 5)
             {
               LevelTemplate levelTemplate =  CreateLevelTemplate<LevelTemplate>("Level_Easy_" + (i+1) +".asset","Assets/LevelTemplate/Easy");
               levelTemplate.ropes = lines[i];
                EditorUtility.SetDirty(levelTemplate);

             }

            else
             {
                LevelTemplate levelTemplate = CreateLevelTemplate<LevelTemplate>("Level_Normal_" + ((i+1) - 5) + ".asset", "Assets/LevelTemplate/Normal");
                levelTemplate.ropes = lines[i];
                EditorUtility.SetDirty(levelTemplate);
            }

        }

        AssetDatabase.SaveAssets();
    }



    [Button("POPULATE EASY LEVELS")]
    public void PopulateEasy()
    {
        string[] path = new string[] { "Assets/LevelTemplate/Easy" };
        _easyLevels = GetAllInstances<LevelTemplate>(path).ToList();
    }

    [Button("POPULATE NORMAL LEVELS")]
    public void PopulateNormal()
    {
        string[] path = new string[] { "Assets/LevelTemplate/Normal" };
        _normalLevels = GetAllInstances<LevelTemplate>(path).ToList();
    }

#endif

}