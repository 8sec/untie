﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RB_UnlockDance : RewardedButton
{
    public override void CollectReward()
    {
        base.CollectReward();
        MysteryManager.Instance.UnlockMysteryGift();
        Debug.Log("A");
    }
}
