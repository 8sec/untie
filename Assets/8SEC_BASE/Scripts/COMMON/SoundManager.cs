﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour {

    public static SoundManager Instance;
    public bool IsMuted;

    public AudioSource AmbiantSound;
    public AudioSource MenuSoundAudioSource;

    [Header ("Menu, Title & UI")]
    public AudioClip LaunchSound;
    public AudioClip PressStartSound;
    public AudioClip GameOver;
    public AudioClip ClickSound;
    public AudioClip SwitchTabSound;
    public AudioClip BackSound;
    public AudioClip Score;

    [Header ("Pinballz")]
    public AudioClip PaddleUp;
    public AudioClip PaddleDown;
    public AudioClip NewBall;
    public AudioClip LostBall;

    public AudioClip PatternClear;
    public AudioClip LevelUp;

    [Header ("Upgrades")]
    public AudioClip ShowShop;
    public AudioClip HideShop;
    public AudioClip BuyUpgrade;
    public AudioClip CollectOfflineCash;
    public AudioClip FailedUpgrade;
    public AudioClip SpawnBall;
    [Header ("Pins")]
    public List<AudioClip> PinSounds = new List<AudioClip> ();

    public AudioClip PinSound {
        get {
            return PinSounds[Random.Range (0, PinSounds.Count)];
        }
    }

    public AudioClip PinImpact;
    public List<AudioClip> PinSoundExplosions = new List<AudioClip> ();

    public AudioClip PinSoundExplosion {
        get {
            return PinSoundExplosions[Random.Range (0, PinSoundExplosions.Count)];
        }
    }

    public List<AudioClip> StrikeSounds = new List<AudioClip> ();

    public AudioClip StrikeSound {
        get {
            return StrikeSounds[Random.Range (0, StrikeSounds.Count)];
        }
    }

    public AudioClip MayhemSound;
    public AudioClip ComboSound;
    public AudioClip StartFireSound;
    public AudioClip EndFireSound;
    public AudioClip PinFireSound;

    public AudioClip LevelUpSound;
    public AudioClip HighScore;
    public AudioClip HighScoreGameOver;
    public AudioClip BreakObstacle {
        get {
            return BreakObstacleSounds[Random.Range (0, BreakObstacleSounds.Count)];
        }
    }
    public List<AudioClip> BreakObstacleSounds = new List<AudioClip> ();

    public AudioClip BonusLetterCollect;

    public AudioSource CircleSource;

    public AudioClip LevelSuccess;

    public void SetVolume (float f) {
        if (MenuSoundAudioSource != null)
            MenuSoundAudioSource.volume = f;
        if (AmbiantSound != null)
            AmbiantSound.volume = f;

    }

    public void RestoreSavedVolume () {

        if (MenuSoundAudioSource != null)
            MenuSoundAudioSource.volume = UserConfig.SoundFXVolume;

        if (AmbiantSound != null)
            AmbiantSound.volume = UserConfig.SoundFXVolume;

    }

    void Awake () {
        Instance = this;

        //		MenuSoundAudioSource = GetComponent<AudioSource> ();
        //if(MenuSoundAudioSource == null)
        //Debug.LogError ("[SoundManager] Please attach an AudioSource component to the SoundManager gameobject");
        //Debug.LogError ("[SoundManager] Update checks timescale");
    }

    public void SetPitch (float p) {
        MenuSoundAudioSource.pitch = p;
    }

    void Start () {
        RestoreSavedVolume ();
        if (UserConfig.SoundFX)
            MuteButton.sprite = Unmuted;
        else
            MuteButton.sprite = Muted;

        if (PlayerPrefs.GetInt("IsMuted") == 0)
        {
            IsMuted = false;
        }

        else
        {
            IsMuted = true;
        }
    }

    float lastPitch = 1f;

    public void Play (AudioClip clip) {
        if (!UserConfig.SoundFX) {
            //Debug.Log("[SoundManager] SoundFX are disabled in UserConfig. Aborting sound play.");
            return;
        }
        //Debug.Log ("Playing...");
        if (MenuSoundAudioSource != null && clip != null) {

            MenuSoundAudioSource.PlayOneShot (clip);
        }
    }

    private float lastImpactTime = 0f;
    private float lastExplosionTime = 0f;
    private float currentPitch = 1f;

    public void PlaySolo (AudioClip clip) {
        if (!UserConfig.SoundFX) {
            //Debug.Log("[SoundManager] SoundFX are disabled in UserConfig. Aborting sound play.");
            return;
        }

    }

    public void PlayInLoop (AudioClip clip) {
        if (!UserConfig.SoundFX) {
            //Debug.Log("[SoundManager] SoundFX are disabled in UserConfig. Aborting sound play.");
            return;
        }
        //Debug.Log ("Looping...");
        if (MenuSoundAudioSource != null && clip != null) {
            //Debug.Log("with source and clip : "+clip.name);
            MenuSoundAudioSource.loop = true;
            MenuSoundAudioSource.clip = clip;
            MenuSoundAudioSource.Play ();
        }
    }

    public void StopLooping () {
        MenuSoundAudioSource.loop = false;
        MenuSoundAudioSource.Stop ();
    }

    public void PlayAtVolume (AudioClip clip, float volume) {
        if (!UserConfig.SoundFX) {
            //Debug.Log("[SoundManager] SoundFX are disabled in UserConfig. Aborting sound play.");
            return;
        }
        float currentVolume = MenuSoundAudioSource.volume;
        MenuSoundAudioSource.volume = volume;
        if (MenuSoundAudioSource != null && clip != null) {
            MenuSoundAudioSource.PlayOneShot (clip);
        }
        MenuSoundAudioSource.volume = currentVolume;
    }

    public void PlayMenuSound(AudioClip menuClip)
    {
        if (IsMuted == false)
        {
            MenuSoundAudioSource.PlayOneShot(menuClip);
        }
    }

    public void PlayClickSound()
    {
        if (IsMuted == false)
        {
            MenuSoundAudioSource.PlayOneShot(ClickSound);
        }
    }

    public void PlayScoreSound()
    {
        if (IsMuted == false)
        {
          //  MenuSoundAudioSource.PlayOneShot(Score);
        }
    }

    public void PlaySwitchTabSound()
    {
        if (IsMuted == false)
        {
            MenuSoundAudioSource.PlayOneShot(SwitchTabSound);
        }
    }

    public void PlayBuySound()
    {
        if (IsMuted == false)
        {
            MenuSoundAudioSource.PlayOneShot(BuyUpgrade);
        }
    }

    public void PlayBackSound()
    {
        if (IsMuted == false)
        {
            MenuSoundAudioSource.PlayOneShot(BackSound);
        }
    }

    public UnityEngine.UI.Image MuteButton;
    public Sprite Unmuted;
    public Sprite Muted;

    public void ToggleSound () {
        UserConfig.SetSoundFX (!UserConfig.SoundFX);
        if (UserConfig.SoundFX) {
            MuteButton.sprite = Unmuted;
            Play (BuyUpgrade);
        } else
        {
            MuteButton.sprite = Muted;
        }
    }

    [Header ("Background Music")]
    public AudioSource BackgroundMusicSource;
    public float DefaultBackgroundMusicVolume = 0.35f;

    public void SetAmbiantVolume (float v) {
        UserConfig.BackgroundMusicVolume = v;
        if (BackgroundMusicSource != null)
            BackgroundMusicSource.volume = UserConfig.BackgroundMusicVolume;
    }

    public void ToggleBackgroundMusic () {
        if ( BackgroundMusicSource.volume > 0f) {
            SetAmbiantVolume (0f);
            PlayerPrefs.SetInt("IsMuted", 1);
            IsMuted = true;
        } else {
            SetAmbiantVolume (DefaultBackgroundMusicVolume);
            IsMuted = false;
            PlayerPrefs.SetInt("IsMuted", 0);

        }

    }
}