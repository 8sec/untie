﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Level", menuName = "Untie/Level", order = 1)]
public class LevelTemplate : ScriptableObject
{
    public string ropes;
    public bool hasAnchor;
    public bool HasObstacle => obstacleInfos.obstacleIndex >= 0;
    public bool HasCoinCurve => curveInfos.curveIndex >= 0;
    public bool HasCustomGround => groundIndex > 0;
    public bool a;

    
    public int groundIndex = 0;
    

    [ShowIf("hasAnchor")]
    public bool _hasAnchorRopeOneA;
    [ShowIf("hasAnchor")]
    public bool _hasAnchorRopeOneB;
    [ShowIf("hasAnchor")]
    public bool _hasAnchorRopeTwoA;
    [ShowIf("hasAnchor")]
    public bool _hasAnchorRopeTwoB;
    [ShowIf("hasAnchor")]
    public bool _hasAnchorRopeThreeA;
    [ShowIf("hasAnchor")]
    public bool _hasAnchorRopeThreeB;
    [ShowIf("hasAnchor")]
    public bool _hasAnchorRopeFourA;
    [ShowIf("hasAnchor")]
    public bool _hasAnchorRopeFourB;
    [ShowIf("hasAnchor")]
    public bool _hasAnchorRopeFiveA;
    [ShowIf("hasAnchor")]
    public bool _hasAnchorRopeFiveB;

    [ShowIf("HasObstacle")]
    public ObstacleInfo obstacleInfos;

   // [ShowIf("HasCoinCurve")]
    public CurveInfos curveInfos;



}
