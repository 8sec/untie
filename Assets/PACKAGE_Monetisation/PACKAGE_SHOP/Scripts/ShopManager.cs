﻿using System.Collections;
using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;
using System.Linq;
using Sirenix.Serialization;
using GameAnalyticsSDK;

public class ShopManager : MonoBehaviourSingleton<ShopManager>
{

    #region Events
    public delegate void StoreItemUnlocked(ShopItem unlockedItem);
    public static event StoreItemUnlocked OnItemUnlocked;

    public delegate void OnShopUpdate();
    public static event OnShopUpdate OnShopUpdateEvent;

    public delegate void OnShopExit();
    public static event OnShopExit OnShopExitEvent;

    public delegate void OnShopOpened();
    public static event OnShopOpened OnShopOpenedEvent;
    #endregion

    #region Fields
    [TabGroup("Shop Params")]
    [SerializeField] private bool _unlockRandomPerCategories;
    [TabGroup("Shop Params")]
    [SerializeField] private List<ItemCategory> _itemCategories = new List<ItemCategory>();
    [TabGroup("Shop Params")]
    [SerializeField] private List<int> _unlockPrice = new List<int>();
    [TabGroup("Shop Params")]
    [SerializeField] private GameObject SHOP_WORLD_GO;



    [TabGroup("Tabs")]
    [SerializeField] private Color _tabSelectedColor;
    [TabGroup("Tabs")]
    [SerializeField] private Color _tabUnselectedColor;
    [TabGroup("Tabs")]
    [SerializeField] private List<ShopTab> _shopTabs = new List<ShopTab>();


    [TabGroup("Unlock Animation Settings")]
    [SerializeField] private int _randomMovesBeforeUnlocking = 10;
    [TabGroup("Unlock Animation Settings")]
    [SerializeField] private float _randomAnimationDuration = 3f;
    [TabGroup("Unlock Animation Settings")]
    [SerializeField] private LeanTweenType _randomAnimationEase;

    #endregion

    #region Private Variables

    private ShopTab _currentlySelectedTab;
    private ItemCategory _currentCategorySelected;
    private bool _canInteract = true;
    private List<ShopItem> _availableItems = new List<ShopItem>();
    private List<ShopItem> _unlockableItems = new List<ShopItem>();
    private List<string> ids = new List<string>();
    private int randomMoves = 0;

    #endregion

    #region Getters
    public bool UnlockRandomPerCategories => _unlockRandomPerCategories;
    public bool CanInteract => _canInteract;
    public ItemCategory CurrentCategorySelected => _currentCategorySelected;
    public List<ShopItem> AvailableItems => _availableItems;
    public List<ShopItem_Spawnable> NotUnlockedMysteryItem
    {
        get
        {
            List<ShopItem_Spawnable> spawnable = new List<ShopItem_Spawnable>();

            for (int i = 0; i < _availableItems.Count; i++)
            {
                if (_availableItems[i] is ShopItem_Spawnable)
                {
                    spawnable.Add(_availableItems[i] as ShopItem_Spawnable);
                }
            }

            List<ShopItem_Spawnable> mysteryList = spawnable.FindAll(item => item.IsMystery == true && item.IsUnlocked == false);

            return mysteryList;
        }
    }
    #endregion

    private Dictionary<ShopCategoryItem, string> _saveDic = new Dictionary<ShopCategoryItem, string>()
    {
        [ShopCategoryItem.ANIMATION] = "AnimationKey",
        [ShopCategoryItem.Handle] = "HatKey",
        [ShopCategoryItem.Material] = "ColorKey"
    };

    public Dictionary<ShopCategoryItem, string> SaveDictionnary => _saveDic;



    private void Awake()
    {
        ShopTab.OnTabSelectedEvent += OnTabSelected;
    }

    private void OnDestroy()
    {
        ShopTab.OnTabSelectedEvent -= OnTabSelected;
    }

    void Start()
    {
        InitializeShop();
        UIController.Instance.HidePanelInstantly(UIPanelName.SHOP);
    }

    private void OnEnable()
    {
        SHOP_WORLD_GO.SetActive(true);
    }

    private void OnDisable()
    {
        SHOP_WORLD_GO.SetActive(false);
    }

    public void OpenShop()
    {
        OnShopOpenedEvent?.Invoke();
        UIController.Instance.ShowPanelInstantly(UIPanelName.SHOP);
    }

    public ShopItem GetCurrentlySelectedItem(ShopCategoryItem categoryType)
    {
        ItemCategory category = null;

        for (int i = 0; i < _itemCategories.Count; i++)
        {
            if (_itemCategories[i].ShopItemCategory == categoryType)
            {
                category = _itemCategories[i];
            }
        }

        if (category != null)
        {
            return category.CurrentlySelectedItem;
        }

        else
        {
           // Debug.LogError("THERE IS NO SHOP CATEGORY OF TYPE " + categoryType);
            return null;
        }
    }

    public int UnlockedItems
    {
        get
        {
            int res = 0;
            foreach (var item in _availableItems)
            {
                if (item.IsUnlocked)
                    res++;
            }
            return res;
        }
    }

    public int CurrentUnlockPrice
    {
        get
        {
            int res = UnlockedItems;
            return _unlockPrice[Mathf.Clamp(res-1, 0, _unlockPrice.Count - 1)];
        }
    }

    [Button("Reset Shop Preferences", ButtonSizes.Large)]
    public void ResetShopPrefs()
    {
        ItemCategory[] categories = FindObjectsOfType<ItemCategory>();
        for (int i = 0; i < categories.Length; i++)
        {
            _itemCategories[i].DeletePrefs();
        }
    }

    public ShopItem GetRandomItemNotUnlocked(ShopCategoryItem itemCategory)
    {
        _unlockableItems.Clear();
        ItemCategory category = _itemCategories.Find(itemCat => itemCat.ShopItemCategory == itemCategory);

        foreach (ShopItem item in category.Items)
        {
            if (item.IsUnlocked == false)
            {
                _unlockableItems.Add(item);
            }
        }

        _unlockableItems.Shuffle();

        if (_unlockableItems.Count > 0)
        {
            Debug.LogError("BEST REWARD IN CHEST ROOM : " + _unlockableItems[0]);
            return _unlockableItems[0];
        }

        else
        {
            Debug.LogError("NO MORE SKINS TO UNLOCK IN CATEGORY " + itemCategory);
            return null;
        }
    }

    public void UnlockRandomItem()
    {
        if (_unlockRandomPerCategories == true)
        {
            // TODO séparer les unlocked dans différentes listes en fonction de leur catégorie.
            if (UnlockedItems >= _availableItems.Count)
            {
                Debug.LogError("No more items to unlock ! Aborting.");
                GameAnalytics.NewDesignEvent("ALL ITEMS OF CATEGORY " + _itemCategories + " UNLOCKED");
                return;
            }
        }

        else
        {
            if (UnlockedItems >= _availableItems.Count)
            {
                GameAnalytics.NewDesignEvent("ALL ITEMS ARE UNLOCKED");
                Debug.LogError("No more items to unlock ! Aborting.");
                return;
            }
        }
       

        _canInteract = false;
        _unlockableItems.Clear();
        if (_unlockRandomPerCategories == true)
        {
            foreach (var item in _currentCategorySelected.Items)
            {
                if (!item.IsUnlocked)
                    _unlockableItems.Add(item);
            }

            if (_unlockableItems.Count <= 0)
            {
                Debug.LogError("NO MORE ITEMS TO UNLOCK IN THIS SECTION");
                _canInteract = true;
                return;
            }
        }

        else
        {
            foreach (var item in _availableItems)
            {
                if (!item.IsUnlocked)
                    _unlockableItems.Add(item);
            }
        }

        if (_unlockableItems.Count > 1)
        {
            _unlockableItems.Shuffle();
            randomMoves = Mathf.Min(_randomMovesBeforeUnlocking, _unlockableItems.Count * 2);
            lastT = -1;
            LeanTween.cancel(gameObject);
            LeanTween.value(gameObject, unlockRandomAnimPart, (float)randomMoves, 0F, _randomAnimationDuration).setEase(_randomAnimationEase).setOnComplete(c => {
                _unlockableItems[lastT % _unlockableItems.Count].Unlock();
                _unlockableItems[lastT % _unlockableItems.Count].Select();
                _unlockableItems[lastT % _unlockableItems.Count].TryEquip();
                _canInteract = true;
                if (OnItemUnlocked != null)
                    OnItemUnlocked(_unlockableItems[lastT % _unlockableItems.Count]);
            });

        }
        else
        {
            _unlockableItems[0].Unlock();
            _unlockableItems[0].Select();
            _unlockableItems[0].TryEquip();
            _canInteract = true;
            if (OnItemUnlocked != null)
                OnItemUnlocked(_unlockableItems[0]);


        }



    }
    int lastT = -1;
    void unlockRandomAnimPart(float t)
    {
        if((int)t!=lastT)
        {
            lastT = (int)t;
            //DeselectAll();
            _unlockableItems[(int)t % _unlockableItems.Count].Select();
        }
    }

    public void TryUnlockRandom()
    {
        if (_canInteract)
        {
            {
                bool hasPurchased = CurrencyManager.Instance.RemoveCurrency(CurrentUnlockPrice);

                if (hasPurchased)
                {
                    UnlockRandomItem();
                }
            }
        }
    }

    private void InitializeShop()
    {
        for (int i = 0; i < _itemCategories.Count; i++)
        {
            _itemCategories[i].Init();
            _availableItems.AddRange(_itemCategories[i].Items);
        }

        if (_currentlySelectedTab == null)
        {
            if (_shopTabs.Count > 0)
            {
                OnTabSelected(_shopTabs[0]);
            }
        }
    }

    public void UpdateShop()
    {
        OnShopUpdateEvent?.Invoke();
    }

    private void OnTabSelected(ShopTab selectedTab)
    {
        if (_shopTabs.Count == 0)
        {
            return;
        }

        if (_currentlySelectedTab != null)
        {
            _currentlySelectedTab.SetColor(_tabUnselectedColor);
        }

        selectedTab.SetColor(_tabSelectedColor);
        _currentlySelectedTab = selectedTab;

        if (_currentCategorySelected != null)
        {
            _currentCategorySelected.gameObject.SetActive(false);
            _currentCategorySelected = null;
        }

        _currentCategorySelected = _itemCategories.Find(itemCat => itemCat.ShopItemCategory == selectedTab.TabCategory);

        if (_currentCategorySelected == null)
        {
            Debug.LogError("THERE IS NO SHOP CATEGORY MATCHING THE SELECTED TAB");
        }

        else
        {
            _currentCategorySelected.gameObject.SetActive(true);
        }
    }

    public void OnShopExitButton()
    {
        OnShopExitEvent?.Invoke();
    }

  
}
