﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using Sirenix.OdinInspector;

public class CharacterMovement: MonoBehaviour
{

    public static CharacterMovement Instance;

    public string AnimatorMovementParameterName = "SpeedRun";

    [Header("SPHERE CAST PARAMS FOR GROUND DETECTION")]
    [SerializeField] private float _detectionRadius;
    [SerializeField] private float _SpeedWallSlide;
    [Space]
    [Header("---------------------")]

     

    public Transform infoPlayer;
    [ShowIf("infoPlayer")]
    public Text nameText;
    private string namePlayer;
    public void EditnamePlayer(string name)
    {
        nameText.text = namePlayer = name;
    }

    public float forceMovementMax;
    public float currentForce;
    public float CurrentMovementSpeedRatio
    {
        get
        {
            return currentForce ;
        }
    }
    public float speedRun;

    public float timeToMaxSpeed;
    public float timeToMinSpeed;

    public LeanTweenType StartMoveSpeedEase;
    public LeanTweenType StopMoveSpeedEase;

    public float rotationSpeedInDegrees;

    private float currentYAngle;

    public float characterGravityForce = 50f;

    public Collider characterCollider;

    public Animator animatorController;

    private int layerMask;

    public Rigidbody[] rigdRagdoll;
    private bool fall = false;

    public GameObject arrowDirection;


    public ParticleSystem smokeBehind;

    public bool canMove;

    private Rigidbody rigd;
    public Rigidbody r
    {
        get
        {
            return rigd;
        }
    }

    private void Awake()
    {
        Instance = this;
    }

    private Animator currentAnimator;


    public void Initialize()
    {
        Instance = this;

        currentForce = 0;

        currentYAngle = transform.eulerAngles.y;

        rigd = GetComponent<Rigidbody>();
        currentAnimator = GetComponent<Animator>();
        rigd.isKinematic = false;
        rigd.useGravity = true;

        int layerTMp = LayerMask.NameToLayer("Ground");
        layerMask = 1 << layerTMp;

        ManageGravityRagdoll(false);

        for (int i = 0; i < rigdRagdoll.Length; i++)
            Physics.IgnoreCollision(GetComponent<CapsuleCollider>(), rigdRagdoll[i].GetComponent<Collider>(), true);

        if (nameText != null)
            nameText.gameObject.SetActive(false);

        if (smokeBehind != null)
            smokeBehind.Stop();

        currentAnimator = animatorController;

        CancelInvoke();
        //InvokeRepeating("SaveLastSafePosition", 0.02f, 0.02f);

        //Control_Movement.Instance.scriptPlayer = this;
    }

    private RaycastHit hit;
    private RaycastHit hitMiddle;
    private RaycastHit hitBack;
    private RaycastHit hitFront;
    void Update()
    {
        infoPlayer.forward = Camera.main.transform.forward;



        //Check Under Him
        // Does the ray intersect any objects excluding the player layer
        if (!Physics.Raycast(transform.position + (Vector3.up * 2), -transform.up, out hit, 5f, layerMask))
        {

            //HP_GameManager.Instance.RepositionPlayer();
            arrowDirection.transform.up = hitMiddle.normal;

            return;

            fall = true;

            infoPlayer.gameObject.SetActive(false);
            GetComponent<CapsuleCollider>().enabled = false;
            rigd.useGravity = true;
            rigd.constraints = RigidbodyConstraints.None;

            animatorController.enabled = false;
            ManageGravityRagdoll(true);
            arrowDirection.SetActive(false);

            Invoke("EndPlayer", 2);
        }
        else
        {
            //SaveLastSafePosition();
        }



        if (moving)
            Move();
    }

    private void EndPlayer()
    {
        Destroy(gameObject);
    }

    public Transform offsetParent;
    public Vector3 lastSafePosition;
    public Transform RepositionTransform;
    private void FixedUpdate()
    {
        if (!Physics.Raycast(transform.position + (Vector3.up * 2), -transform.up, out hit, 5f, layerMask) && canMove)
        {
//            Debug.LogError("Reposition");
            //Debug.Break();
        }
        SaveLastSafePosition();

        if (!fall && canMove)
        {
       
            //rotation = Quaternion.AngleAxis(currentYAngle, Vector3.up);
            transform.rotation = Quaternion.RotateTowards(transform.rotation, Quaternion.identity, rotationSpeedInDegrees * Time.fixedDeltaTime);
            //Quaternion arrowNewDir = arrowDirection.transform.rotation;
            //arrowNewDir.y = HP_Player.Instance.CurrentTargetRotation().y;
            //arrowDirection.transform.rotation = Quaternion.RotateTowards(transform.rotation, arrowNewDir, rotationSpeedInDegrees * Time.fixedDeltaTime);

         

            //float speedRatio = 1f;// Mathf.Clamp01(((TargetMove.transform.position - transform.position).sqrMagnitude)) / 1F;
            //rigd.velocity = transform.forward * (currentForce * forceMovementMax * speedRatio) + new Vector3(0,rigd.velocity.y,0);

            rigd.velocity = transform.forward * 10 + new Vector3(0, rigd.velocity.y, 0);
            //rigd.AddForce(transform.forward * HP_Player.Instance.CurrentMovementForce() * 10f, ForceMode.Force);


            offsetParent.transform.rotation = Quaternion.identity;
        }

        // s'il y a du sol en dessous de soi, on tombe
        if (!Physics.Raycast(transform.position + (Vector3.up * 2), -transform.up, out hit, 3.5f, layerMask) && canMove)
        {
            rigd.AddForce(Vector3.down * characterGravityForce, ForceMode.Force);
        }


    }

    private Quaternion rotation;
    private int idTweenRot;
    private int idTweenSpeed;
    bool moving = false;
    public Transform TargetMove;
    public float MoveMultiplier;

    public Rigidbody Rigd { get => rigd; set => rigd = value; }

    public void Move()
    {
        //Vector3 dir = TargetMove.transform.position - transform.position;
        //currentYAngle = Mathf.Atan2(dir.x, dir.z) * Mathf.Rad2Deg;
    }

    void SaveLastSafePosition()
    {

        RaycastHit[] sphereCastHits;
        // si on a un collider sous les pieds, on enregistre la dernière position safe
        //  if (Physics.Raycast(transform.position + (Vector3.up * 2), -transform.up, out hitMiddle, Mathf.Infinity, layerMask))
        //  {
        //      HP_GameManager.Instance.lastPlayerSafePosition = transform.position;
        //  }
        sphereCastHits = Physics.SphereCastAll(transform.position + (Vector3.up * 2), _detectionRadius, -transform.up, Mathf.Infinity, layerMask);
        if(RepositionTransform==null)
            RepositionTransform=transform;
        if (sphereCastHits.Length > 0)
        {
            Vector3 closestPoint;
            float closestDist;
            Vector3 tmpClose;
            float tmpDist;
            closestPoint = sphereCastHits[0].collider.ClosestPoint(RepositionTransform.position + rigd.velocity.normalized * _SpeedWallSlide);

            closestDist = Vector3.Distance(transform.position, closestPoint);
            for (int i = 1; i < sphereCastHits.Length; i++)
            {
                tmpClose = sphereCastHits[i].collider.ClosestPoint(RepositionTransform.position + rigd.velocity.normalized * _SpeedWallSlide);
                tmpDist = Vector3.Distance(transform.position, tmpClose);
                if (closestDist > tmpDist)
                {
                    closestDist = tmpDist;
                    closestPoint = tmpClose;
                }
            }
            // Debug.LogError("TOUCHING GROUND" + " " + sphereCastHits.Length);
        }


    }

    public void ResetTarget()
    {
        TargetMove.transform.localPosition = Vector3.zero;
    }

    public void SetTarget(Vector3 direction, float force)
    {
        if (!moving)
        {
            if (smokeBehind != null && !smokeBehind.IsAlive())
                smokeBehind.Play();

            moving = true;
            LeanTween.cancel(idTweenSpeed);
            //   idTweenSpeed = LeanTween.value(gameObject, ChangeSpeed, currentForce, force, timeToMaxSpeed).setIgnoreTimeScale(true).setEase(StartMoveSpeedEase).id;
           
        }
        currentForce = force;
        Vector3 lp = TargetMove.transform.localPosition + direction * MoveMultiplier;
        lp = Vector3.ClampMagnitude(lp, 1f);
        TargetMove.transform.localPosition = lp;
    }

    public void StopMove()
    {
    
        if(smokeBehind != null)
            smokeBehind.Stop();

        if (moving)
        {
            moving = false;
            LeanTween.cancel(idTweenSpeed);
            idTweenSpeed = LeanTween.value(gameObject, ChangeSpeed, currentForce, 0, timeToMinSpeed).setEase(StopMoveSpeedEase).setIgnoreTimeScale(true).id;
        }
    }

    public void ChangeSpeed(float t)
    {
        currentForce = t;
    }

    public void ManageGravityRagdoll(bool activate)
    {
        for (int i = 0; i < rigdRagdoll.Length; i++)
            rigdRagdoll[i].isKinematic = !activate;
    }

    public void Success()
    {
        canMove = false;
        animatorController.SetFloat(AnimatorMovementParameterName, 0f);
        rigd.useGravity = false;
        arrowDirection.SetActive(false);
        characterCollider.enabled = false;
    }
}
