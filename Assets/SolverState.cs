﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;
public class SolverState : ScriptableObject
{
    public ObiNativeVector4List renderablePositions;
    public ObiNativeVector4List velocities;
    public ObiNativeVector4List angularVelocities;
}
